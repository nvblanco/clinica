using System;
using System.Windows.Forms;
using Gestion_tratamientos.View;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos
{
	/// <summary>
	/// Main de la aplicación.
	/// </summary>
	class MainClass
	{
		[STAThread]
		public static void Main (string[] args)
		{
			MainWindow mw = new MainWindow (new Controlador());
			Application.Run (mw);
		}
	}
}
