using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Gestion_tratamientos.BD;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase que sirve como intermediaria entre la vista y la lógica de negocio. 
	/// </summary>
	public class Controlador
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase Controlador.
		/// </summary>
		public Controlador() {
			GestionBD bd = new GestionBD ();

			SortedDictionary<int,Paciente> diccionarioPacientes = bd.SetUpPacientes ();
			gestionpacientes = new GestionPacientes(diccionarioPacientes);

			SortedDictionary<int,Enfermedad> diccionarioEnfermedades = bd.SetUpEnfermedades ();
			gestionenfermedades = new GestionEnfermedades(diccionarioEnfermedades);

			SortedDictionary<int,Medicamento> diccionarioMedicamentos = bd.SetUpMedicamentos ();
			gestionmedicamentos = new GestionMedicamentos(diccionarioMedicamentos);

			SortedDictionary<int,Tratamiento> diccionarioTratamientos = bd.SetUpTratamientos ();
			gestiontratamientos = new GestionTratamientos(diccionarioTratamientos);
		}

		/// <summary>
		/// Método que devuelve una lista con los objetos de tipo Paciente.
		/// </summary>
		/// <returns>Lista con los objetos de tipo Paciente.</returns>
		public List<Paciente> ListarPacientes() {
			return gestionpacientes.Listar();
		}


		/// <summary>
		/// Método que devuelve el objeto de tipo Paciente cuyo id se pasa por parámetro.
		/// </summary>
		/// <returns>Objeto de tipo Paciente.</returns>
		/// <param name="id">Id del Paciente que se quiere recuperar.</param>
		public Paciente ObtenerPaciente(int id) {
			return gestionpacientes.Obtener(id);
		}

		/// <summary>
		/// Método que inserta en el diccionario de pacientes el objeto de tipo Paciente que se pasa como parámetro.
		/// </summary>
		/// <param name="p">Objeto de tipo Paciente.</param>
		public void InsertarPaciente(Paciente p) {
			gestionpacientes.Insertar(p);
		}

		/// <summary>
		/// Método que recibe el id de una enfermedad y un objeto de tipo Core.Paciente y actualiza los datos de dicho objeto en el diccionario de pacientes.
		/// </summary>
		/// <param name="id">Id del paciente.</param>
		/// <param name="pNuevo">Objeto de tipo Core.Paciente.</param>
		public void ModificarPaciente(int id, Paciente pNuevo) {
			gestionpacientes.Modificar(id, pNuevo);
		}

		/// <summary>
		/// Método que borra del diccionario de pacientes los datos del objeto de tipo Core.Paciente cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del paciente a borrar.</param>
		public void BorrarPaciente(int id) {
			foreach(Tratamiento t in gestiontratamientos.Listar()) {
				if(t.Paciente.Id == id) {
					gestiontratamientos.Borrar(t.Id);
				}
			}

			gestionpacientes.Borrar(id);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Paciente presentes en el diccionario de pacientes.
		/// </summary>
		/// <value>El número de objetos de tipo Paciente presentes en el diccionario de pacientes.</value>
		public int NumPacientes {
			get {
				return gestionpacientes.Count;
			}
		}

		/// <summary>
		/// Método que devuelve una lista con los objetos de tipo Enfermedad.
		/// </summary>
		/// <returns>Lista con los objetos de tipo Enfermedad.</returns>
		public List<Enfermedad> ListarEnfermedades() {
			return gestionenfermedades.Listar();
		}

		/// <summary>
		/// Método que devuelve el objeto de tipo Enfermedad cuyo id se pasa por parámetro.
		/// </summary>
		/// <returns>Objeto de tipo Enfermedad.</returns>
		/// <param name="id">Id de la Enfermedad que se quiere recuperar.</param>
		public Enfermedad ObtenerEnfermedad(int id) {
			return gestionenfermedades.Obtener (id);
		}

		/// <summary>
		/// Método que inserta en el diccionario de enfermedades el objeto de tipo Enfermedad que se pasa como parámetro.
		/// </summary>
		/// <param name="e">Objeto de tipo Enfermedad.</param>
		public void InsertarEnfermedad(Enfermedad e) {
			gestionenfermedades.Insertar (e);
		}

		/// <summary>
		/// Modifica en el diccionario de enfermedades la enfermedad cuyo id se pasa por parámetro con los datos del objeto de tipo Enfermedad pasado como parámetro
		/// </summary>
		/// <param name="id">Id de la enfermedad a modificar.</param>
		/// <param name="eNueva">Objeto de tipo Enfermedad que contiene los datos modificados.</param>
		public void ModificarEnfermedad(int id, Enfermedad eNueva) {
			gestionenfermedades.Modificar (id, eNueva);
		}

		/// <summary>
		/// Método que elimina del diccionario de enfermedades el objeto de tipo Enfermedad cuyo id se pasa como parámetro
		/// </summary>
		/// <param name="id">Id de la enfermedad a borrar.</param>
		public void BorrarEnfermedad(int id) {
			Enfermedad e = gestionenfermedades.Obtener(id);

			foreach(Medicamento m in gestionmedicamentos.Listar()) {
				if(m.ListaEnfermedades.Contains(e)) {
					m.ListaEnfermedades.Remove(e);

					if(m.ListaEnfermedades.Count == 0) {
						gestionmedicamentos.Eliminar(m.Id);
					} else {
						gestionmedicamentos.Modificar(m);
					}
				}
			}

			foreach(Tratamiento t in gestiontratamientos.Listar()) {
				if(t.Enfermedad.Id == id) {
					gestiontratamientos.Borrar(t.Id);
				}
			}

			gestionenfermedades.Borrar(id);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Enfermedad presentes en el diccionario de enfermedades.
		/// </summary>
		/// <value>El número de objetos de tipo Enfermedad presentes en el diccionario de enfermedades.</value>
		public int NumEnfermedades {
			get{
				return gestionenfermedades.Count;
			}
		}

		/// <summary>
		/// Método que devuelve una lista con los objetos de tipo Medicamento.
		/// </summary>
		/// <returns>Lista con los objetos de tipo Medicamento.</returns>
		public List<Medicamento> ListarMedicamentos() {
			return gestionmedicamentos.Listar();
		}

		/// <summary>
		/// Método que devuleve la lista de objetos de tipo Medicamento presentes en el diccionario de medicamentos y que cumplan las restricciones pasadas por parámetro.
		/// </summary>
		/// <returns>La lista de objetos de tipo Medicamento resultante.</returns>
		/// <param name="tipo">Tipo de búsqueda (por nombre de medicamento o por enfermedad que trata).</param>
		/// <param name="valor">Valor de la búsqueda.</param>
		public List<Medicamento> ListarMedicamentos(string tipo, string valor) {
			return gestionmedicamentos.Listar(tipo,valor);
		}

		/// <summary>
		/// Método que crea el objeto de tipo Medicamento con los datos recibidos por parámetro y lo inserta en el diccionario de medicamentos.
		/// </summary>
		/// <param name="nombre">Nombre del medicamento.</param>
		/// <param name="listaIdsEnfermedades">Lista de ids correspondientes a las enfermedades que trata el medicamento.</param>
		public void InsertarMedicamento(string nombre, List<int> listaIdsEnfermedades) {
			var listaEnfermedades = new List<Enfermedad>();
			foreach(int id in listaIdsEnfermedades){
				listaEnfermedades.Add (ObtenerEnfermedad (id));
			}
			var medicamento = new Medicamento (nombre, listaEnfermedades);
			gestionmedicamentos.Insertar (medicamento);
		}

		/// <summary>
		/// Modifica en el diccionario de medicamentos el medicamento cuyo id se pasa por parámetro con los datos del objeto de tipo Medicamentos pasado como parámetro.
		/// </summary>
		/// <param name="idMedicamento">Id del medicamento a modificar.</param>
		/// <param name="nombre">Nuevo nombre del medicamento.</param>
		/// <param name="listaIdsEnfermedades">Lista de ids con las enfermedades que trata el medicamento.</param>
		public void ModificarMedicamento(int idMedicamento,string nombre, List<int> listaIdsEnfermedades) {
			var listaEnfermedades = new List<Enfermedad>();
			foreach(int id in listaIdsEnfermedades){
				listaEnfermedades.Add (gestionenfermedades.Obtener (id));
			}
			var medicamento = new Medicamento (nombre, listaEnfermedades);
			medicamento.Id = idMedicamento;
			gestionmedicamentos.Modificar (medicamento);
		}

		/// <summary>
		/// Borra del diccionario de medicamentos el objeto de tipo Medicamento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del medicamento.</param>
		public void BorrarMedicamento(int id) {
			Medicamento m = gestionmedicamentos.Obtener(id);

			foreach(Tratamiento t in gestiontratamientos.Listar()) {
				if(t.ListaMedicamentos.Contains(m)) {
					t.ListaMedicamentos.Remove(m);

					if(t.ListaMedicamentos.Count == 0) {
						gestiontratamientos.Borrar(t.Id);
					} else {
						gestiontratamientos.Modificar(t);
					}
				}
			}

			gestionmedicamentos.Eliminar(id);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Medicamento presentes en el diccionario de medicamentos.
		/// </summary>
		/// <value>El número de objetos de tipo Medicamento presentes en el diccionario de medicamentos.</value>
		public int NumMedicamentos {
			get{
				return gestionmedicamentos.Count;
			}
		}

		/// <summary>
		/// Método que devuelve el objeto de tipo Medicamento cuyo id se pasa por parámetro.
		/// </summary>
		/// <returns>Objeto de tipo Medicamento.</returns>
		/// <param name="id">Id del Medicamento que se quiere recuperar.</param>
		public Medicamento ObtenerMedicamento(int id) {
			return gestionmedicamentos.Obtener(id);
		}

		/// <summary>
		/// Método que devuelve una lista con los objetos de tipo Tratamiento.
		/// </summary>
		/// <returns>Lista con los objetos de tipo Tratamiento.</returns>
		public List<Tratamiento> ListarTratamientos() {
			return gestiontratamientos.Listar();
		}

		/// <summary>
		/// Método que devuelve el objeto de tipo Tratamiento cuyo id se pasa por parámetro.
		/// </summary>
		/// <returns>Objeto de tipo Tratamiento.</returns>
		/// <param name="id">Id de la Tratamiento que se quiere recuperar.</param>
		public Tratamiento ObtenerTratamiento(int id) {
			return gestiontratamientos.Obtener(id);
		}

		/// <summary>
		/// Método que crea el objeto de tipo Tratamiento con los datos recibidos por parámetro y lo inserta en el diccionario de tratamientos.
		/// </summary>
		/// <param name="duracion">Duracion del tratamiento.</param>
		/// <param name="medicamentos">Lista de objetos de tipo Medicamento que representa los medicamentos recetados en el tratamiento.</param>
		/// <param name="enfermedad">Objeto de tipo Enfermedad que representa la enfermedad para la cual se prescribe el tratamiento.</param>
		/// <param name="paciente">Objeto de tipo Paciente que representa el paciente para el cual se prescribe el tratamiento.</param>
		public void InsertarTratamiento(int duracion, List<Medicamento> medicamentos, Enfermedad enfermedad, Paciente paciente) {
			gestiontratamientos.Insertar(new Tratamiento(duracion, medicamentos, enfermedad, paciente));
		}

		/// <summary>
		/// Método que modifica el objeto Tratamiento del diccionario de tratamientos con los datos pasados por parámetro.
		/// </summary>
		/// <param name="id">Id del tratamiento.</param>
		/// <param name="duracion">Duracion del tratamiento.</param>
		/// <param name="medicamentos">Lista de objetos de tipo Medicamento que representa los medicamentos recetados en el tratamiento.</param>
		/// <param name="enfermedad">Enfermedad para la cual se prescribe el tratamiento.</param>
		/// <param name="paciente">Paciente para el cual se prescribe el tratamiento.</param>
		public void ModificarTratamiento(int id, int duracion, List<Medicamento> medicamentos, Enfermedad enfermedad, Paciente paciente) {
			gestiontratamientos.Modificar(new Tratamiento(id, duracion, medicamentos, enfermedad, paciente));
		}

		/// <summary>
		/// Método que borra del diccionario del tratamientos el objeto Tratamiento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del tratamiento a borrar.</param>
		public void BorrarTratamiento(int id) {
			gestiontratamientos.Borrar(id);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Tratamiento presentes en el diccionario de tratamientos.
		/// </summary>
		/// <value>El número de objetos de tipo Tratamiento presentes en el diccionario de tratamientos.</value>
		public int NumTratamientos {
			get{
				return gestiontratamientos.Count;
			}
		}

		private GestionMedicamentos gestionmedicamentos;
		private GestionEnfermedades gestionenfermedades;
		private GestionPacientes gestionpacientes;
		private GestionTratamientos gestiontratamientos;
	}
}

