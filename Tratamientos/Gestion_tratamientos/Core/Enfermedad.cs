using System;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase que representa la entidad Enfermedad.
	/// </summary>
	public class Enfermedad
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase Enfermedad.
		/// </summary>
		/// <param name="id">Id de la enfermedad.</param>
		/// <param name="nombre">Nombre de la enfermedad.</param>
		/// <param name="tipo">Tipo de la enfermedad (virus o bacteria).</param>
		public Enfermedad (int id, string nombre, string tipo) {
			Id = id;
			Nombre = nombre;
			Tipo = tipo;
		}

		/// <summary>
		/// Identificador único del objeto Enfermedad.
		/// </summary>
		/// <value>El identificador.</value>
		public int Id {
			get;
			set;
		}

		/// <summary>
		/// Nombre de la Enfermedad.
		/// </summary>
		/// <value>El nombre.</value>
		public string Nombre {
			get;
			set;
		}

		/// <summary>
		/// Tipo de la Enfermedad.
		/// </summary>
		/// <value>El tipo ("virus" o "bacteria").</value>
		public string Tipo {
			get;
			set;
		}

	}
}