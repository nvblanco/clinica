using System;
using System.Collections.Generic;
using Gestion_tratamientos.BD;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, borrados, modificaciones y consultas de objetos de tipo <see cref="Medicamento"/> en memoria.
	/// </summary>
	public class GestionMedicamentos
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase GestionMedicamentos con los medicamentos que se pasan por parámetro.
		/// </summary>
		/// <param name="dictionary">Diccionario de objetos de tipo Medicamento.</param>
		public GestionMedicamentos(SortedDictionary<int, Medicamento> dictionary) {
			this.medicamentos = dictionary;
			this.bd = new GestionBDMedicamentos();
			actualId = getLastId() + 1;
		}

		/// <summary>
		/// Método que inserta en el diccionario de medicamentos el objeto Medicamento pasado como parámetro.
		/// </summary>
		/// <param name="medicamento">El objeto Medicamento a insertar.</param>
		public void Insertar(Medicamento medicamento) {
			medicamento.Id = actualId;
			medicamentos.Add(actualId,medicamento);
			actualId++;
			bd.Insert(medicamento);
		}

		/// <summary>
		/// Método que modifica en el diccionario de medicamentos el objeto Medicamento pasado como parámetro.
		/// </summary>
		/// <param name="medicamento">El objeto Medicamento con los datos que se quieren modificar.</param>
		public void Modificar(Medicamento medicamento) {
			medicamentos.Remove(medicamento.Id);
			medicamentos.Add(medicamento.Id,medicamento);
			bd.Update(medicamento);
		}

		/// <summary>
		/// Método que borra del diccionario de medicamentos el medicamento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id del medicamento a borrar.</param>
		public void Eliminar(int id) {
			medicamentos.Remove(id);
			bd.Delete(id);
		}

		/// <summary>
		/// Método que obtiene un objeto de tipo Medicamento a partir de un id pasado como parámetro.
		/// </summary>
		/// <param name="id">El id del medicamento a recuperar.</param>
		public Medicamento Obtener (int id) {
			return medicamentos [id];
		}

		/// <summary>
		/// Método que devuelve la lista de objetos de tipo Medicamento contenidos en el diccionario de medicamentos.
		/// </summary>
		/// <returns>Lista de objetos Medicamento presentes en el diccionario</returns>
		public List<Medicamento> Listar() {
			return new List<Medicamento>(medicamentos.Values);
		}

		/// <summary>
		/// Método que devuelve una lista de objetos de tipo Medicamento que cumplen las restricciones pasadas por parámetro.
		/// </summary>
		/// <param name="tipoBusqueda">Tipo de búsqueda (por nombre del medicamento o por enfermedades que trata).</param>
		/// <param name="valor">Valor de la búsqueda.</param>
		public List<Medicamento> Listar(string tipoBusqueda, string valor) {
			var lista = this.Listar();
			var toret = this.Listar();

			if(tipoBusqueda == "Nombre") {
				foreach(Medicamento medicamento in lista) {
					if(!medicamento.Nombre.ToLower().Contains(valor.ToLower())) {
						toret.Remove (medicamento);
					}
				}
			} else {
				foreach(Medicamento medicamento in lista) {
					String listadoEnfermedades = medicamento.ListadoEnfermedades();
					if(!listadoEnfermedades.ToLower().Contains(valor.ToLower())) {
						toret.Remove (medicamento);
					}
				}
			}

			return toret;
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Medicamento presentes en el diccionario.
		/// </summary>
		/// <value>El número de medicamentos.</value>
		public int Count {
			get{
				return medicamentos.Count;
			}
		}

		/// <summary>
		/// Método que devuelve el id del último medicamento insertado.
		/// </summary>
		/// <returns>El id del último objeto Medicamento insertado en el diccionario de medicamentos.</returns>
		private int getLastId() {
			int toRet = 0;
			foreach (int id in medicamentos.Keys) {
				toRet = id;
			}
			return toRet;
		}

		private SortedDictionary<int,Medicamento> medicamentos;
		private GestionBDMedicamentos bd;
		private int actualId;
	}
}