using System;
using System.Collections.Generic;
using System.Text;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase que representa la entidad Medicamento.
	/// </summary>
	public class Medicamento
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase Medicamento.
		/// </summary>
		/// <param name="nombre">Nombre del medicamento.</param>
		/// <param name="enfermedades">Lista de objetos de tipo Enfermedad que trata el medicamento.</param>
		public Medicamento (string nombre, List<Enfermedad> enfermedades) {
			this.Nombre = nombre;
			this.ListaEnfermedades = enfermedades;
		}

		/// <summary>
		/// Inicializa una nueva instancia de la clase Medicamento.
		/// </summary>
		/// <param name="id">Id del medicamento</param> 
		/// <param name="nombre">Nombre del medicamento.</param>
		/// <param name="enfermedades">Lista de objetos de tipo Enfermedad que trata el medicamento.</param>
		public Medicamento (int id,string nombre, List<Enfermedad> enfermedades)
		{
			this.Id = id;
			this.Nombre = nombre;
			this.ListaEnfermedades = enfermedades;
		}

		/// <summary>
		/// Nombre del Medicamento.
		/// </summary>
		/// <value>El nombre.</value>
		public string Nombre {
			get;
			set;
		}

		/// <summary>
		/// Identificador único del objeto Medicamento.
		/// </summary>
		/// <value>El identificador.</value>
		public int Id {
			get;
			set;
		}

		/// <summary>
		/// Lista de objetos de tipo Enfermedad que combate el medicamento.
		/// </summary>
		/// <value>Lista de enfermedades.</value>
		public List<Enfermedad> ListaEnfermedades {
			get;
			set;
		}

		/// <summary>
		/// Método que formatea los nombres de las enfermedades que combate el medicamento para ser presentados correctamente.
		/// </summary>
		/// <returns>Cadena de texto con los nombres de las enfermedades.</returns>
		public string ListadoEnfermedades() {
			StringBuilder toret = new StringBuilder ();
			int counter = 0;
			foreach(Enfermedad enfermedad in this.ListaEnfermedades) {
				toret.Append(enfermedad.Nombre);
				if(this.ListaEnfermedades.Count > ++counter) {
					toret.Append(", ");
				}
			}
			return toret.ToString();
		}
	}
}