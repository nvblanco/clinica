using System;
using System.Collections.Generic;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase que representa la entidad Paciente.
	/// </summary>
	public class Paciente
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase Paciente.
		/// </summary>
		/// <param name="id">Id del paciente.</param>
		/// <param name="nombre">Nombre del paciente.</param>
		/// <param name="apellidos">Apellidos del paciente.</param>
		/// <param name="dni">Documento Nacional de Identidad del paciente.</param>
		/// <param name="fechaNac">Fecha de Nacimiento del paciente.</param>
		public Paciente (int id, string nombre, string apellidos, string dni, string fechaNac) {
			Id = id;
			Nombre = nombre;
			Apellidos = apellidos;
			DNI = dni;
			FechaNac = fechaNac;
		}

		/// <summary>
		/// Identificador único del objeto Paciente.
		/// </summary>
		/// <value>El identificador.</value>
		public int Id {
			get;
			set;
		}

		/// <summary>
		/// Nombre del Paciente.
		/// </summary>
		/// <value>El nombre.</value>
		public string Nombre {
			get;
			set;
		}

		/// <summary>
		/// Apellidos del Paciente.
		/// </summary>
		/// <value>Los apellidos.</value>
		public string Apellidos {
			get;
			set;
		}

		/// <summary>
		/// DNI del Paciente.
		/// </summary>
		/// <value>El DNI.</value>
		public string DNI {
			get;
			set;
		}

		/// <summary>
		/// Fecha de nacimiento del paciente.
		/// </summary>
		/// <value>La fecha.</value>
		public string FechaNac {
			get;
			set;
		}
			
	}
}