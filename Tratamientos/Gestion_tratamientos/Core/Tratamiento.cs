using System;
using System.Collections.Generic;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase que representa la entidad Tratamiento.
	/// </summary>
	public class Tratamiento
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase Tratamiento.
		/// </summary>
		/// <param name="duracion_dias">Duracion del tratamiento (en días).</param>
		/// <param name="medicamentos">Lista de objetos de tipo Medicamento prescritos en el tratamiento.</param>
		/// <param name="enfermedad">Enfermedad que combate el tratamiento.</param>
		/// <param name="paciente">Paciente para el cual se prescribe el tratamiento.</param>
		public Tratamiento (int duracion_dias, List<Medicamento> medicamentos, Enfermedad enfermedad, Paciente paciente) {
			Duracion = duracion_dias;
			ListaMedicamentos = medicamentos;
			Enfermedad = enfermedad;
			Paciente = paciente;
		}

		/// <summary>
		/// Inicializa una nueva instancia de la clase Tratamiento.
		/// </summary>
		/// <param name="id">Id del Tratamiento</param> 
		/// <param name="duracion_dias">Duracion del tratamiento (en días).</param>
		/// <param name="medicamentos">Lista de objetos de tipo Medicamento prescritos en el tratamiento.</param>
		/// <param name="enfermedad">Enfermedad que combate el tratamiento.</param>
		/// <param name="paciente">Paciente para el cual se prescribe el tratamiento.</param>
		public Tratamiento (int id, int duracion_dias, List<Medicamento> medicamentos, Enfermedad enfermedad, Paciente paciente){
			Id = id;
			Duracion = duracion_dias;
			ListaMedicamentos = medicamentos;
			Enfermedad = enfermedad;
			Paciente = paciente;
		}

		/// <summary>
		/// Identificador único del objeto Tratamiento.
		/// </summary>
		/// <value>El identificador.</value>
		public int Id {
			get;
			set;
		}

		/// <summary>
		/// Duración del Tratamiento.
		/// </summary>
		/// <value>La duracion.</value>
		public int Duracion {
			get;
			set;
		}

		/// <summary>
		/// Lista de objetos de tipo Medicamento que se prescriben en el tratamiento.
		/// </summary>
		/// <value>La lista de medicamentos.</value>
		public List<Medicamento> ListaMedicamentos {
			get;
			set;
		}

		/// <summary>
		/// Enfermedad que combate el tratamiento.
		/// </summary>
		/// <value>The enfermedad.</value>
		public Enfermedad Enfermedad {
			get;
			set;
		}

		/// <summary>
		/// Paciente para el cual se prescribe el tratamiento.
		/// </summary>
		/// <value>El paciente.</value>
		public Paciente Paciente {
			get;
			set;
		}
	}
}