using System;
using System.Collections.Generic;
using Gestion_tratamientos.BD;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, borrados, modificaciones y consultas de objetos de tipo <see cref="Paciente"/> en memoria.
	/// </summary>
	public class GestionPacientes
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase GestionPacientes con los pacientes que se pasan por parámetro.
		/// </summary>
		/// <param name="dictionary">Diccionario de objetos de tipo Paciente.</param>
		public GestionPacientes(SortedDictionary<int, Paciente> dictionary) {
			this.pacientes = dictionary;
			this.bd = new GestionBDPacientes();
			actualId = getLastId() + 1;
		}

		/// <summary>
		/// Método que obtiene del diccionario el objeto Paciente cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id del paciente a consultar.</param>
		public Paciente Obtener(int id) {
			return pacientes[id];
		}

		/// <summary>
		/// Método que nserta en el diccionario de pacientes el objeto Paciente pasado como parámetro.
		/// </summary>
		/// <param name="p">El objeto Paciente a insertar.</param>
		public void Insertar(Paciente p) {
			p.Id = actualId;
			pacientes.Add(actualId, p);
			actualId++;
			bd.Insert(p);
		}

		/// <summary>
		/// Método que modifica en el diccionario de pacientes el paciente cuyo id se pasa por parámetro con los datos del objeto Paciente pasado como parámetro.
		/// </summary>
		/// <param name="id">El id del paciente a modificar.</param>
		/// <param name="pNuevo">El objeto Paciente con los datos que se quieren modificar.</param>
		public void Modificar(int id, Paciente pNuevo) {
			pacientes[id].Nombre = pNuevo.Nombre;		
			pacientes[id].Apellidos = pNuevo.Apellidos;
			pacientes[id].DNI = pNuevo.DNI;
			pacientes[id].FechaNac = pNuevo.FechaNac;
			bd.Update(id, pNuevo);
		}

		/// <summary>
		/// Método que borra del diccionario de pacientes el paciente cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id del paciente a borrar.</param>
		public void Borrar(int id) {
			pacientes.Remove(id);
			bd.Delete(id);
		}

		/// <summary>
		/// Método que devuelve la lista de objetos de tipo Paciente contenidos en el diccionario de pacientes.
		/// </summary>
		/// <returns>Lista de objetos Paciente presentes en el diccionario</returns>
		public List<Paciente> Listar() {
			return new List<Paciente>(pacientes.Values);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Paciente presentes en el diccionario.
		/// </summary>
		/// <value>El número de pacientes.</value>
		public int Count {
			get {
				return pacientes.Count;
			}
		}

		/// <summary>
		/// Método que devuelve el id del último paciente insertado.
		/// </summary>
		/// <returns>El id del último objeto Paciente insertado en el diccionario de pacientes.</returns>
		private int getLastId() {
			int toRet = 0;
			foreach (int id in pacientes.Keys) {
				toRet = id;
			}
			return toRet;
		}

		private SortedDictionary<int,Paciente> pacientes;
		private GestionBDPacientes bd;
		private int actualId;
	}
}