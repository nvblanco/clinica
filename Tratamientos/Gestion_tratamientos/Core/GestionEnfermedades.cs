using System;
using System.Collections.Generic;
using Gestion_tratamientos.BD;


namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, borrados, modificaciones y consultas de objetos de tipo <see cref="Enfermedad"/> en memoria.
	/// </summary>
	public class GestionEnfermedades
	{

		/// <summary>
		/// Inicializa una nueva instancia de la clase GestionEnfermedades con las enfermedades que se pasan por parámetro.
		/// </summary>
		/// <param name="dictionary">Diccionario de objetos de tipo Enfermedad.</param>
		public GestionEnfermedades(SortedDictionary<int, Enfermedad> dictionary) {
			this.enfermedades = dictionary;
			this.bd = new GestionBDEnfermedades();
			actualId = getLastId() + 1;
		}

		/// <summary>
		/// Método que obtiene un objeto de tipo Enfermedad a partir de un id pasado como parámetro.
		/// </summary>
		/// <param name="id">El id de la enfermedad a recuperar.</param>
		public Enfermedad Obtener(int id) {
			return enfermedades[id];
		}

		/// <summary>
		/// Método que inserta en el diccionario de enfermedades el objeto Enfermedad pasado como parámetro.
		/// </summary>
		/// <param name="e">El objeto enfermedad a insertar.</param>
		public void Insertar(Enfermedad e) {
			e.Id = actualId;
			enfermedades.Add(actualId,e);
			actualId++;
			bd.Insert(e);
		}

		/// <summary>
		/// Método que modifica en el diccionario de enfermedades la enfermedad cuyo id se pasa por parámetro con los datos del objeto Enfermedad pasado como parámetro.
		/// </summary>
		/// <param name="id">El id de la enfermedad a modificar.</param>
		/// <param name="eNueva">El objeto Enfermedad con los datos que se quieren modificar.</param>
		public void Modificar(int id, Enfermedad eNueva) {
			enfermedades[id].Nombre = eNueva.Nombre;		
			enfermedades[id].Tipo = eNueva.Tipo;
			bd.Update(id, eNueva);
		}

		/// <summary>
		/// Método que borra del diccionario de enfermedades la enfermedad cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id de la enfermedad a borrar.</param>
		public void Borrar(int id) {
			enfermedades.Remove(id);
			bd.Delete(id);
		}

		/// <summary>
		/// Método que devuelve la lista de objetos de tipo Enfermedad contenidos en el diccionario de enfermedades.
		/// </summary>
		/// <returns>Lista de objetos Enfermedad presentes en el diccionario</returns>
		public List<Enfermedad> Listar() {
			return new List<Enfermedad>(enfermedades.Values);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Enfermedad presentes en el diccionario.
		/// </summary>
		/// <value>El número de enfermedades.</value>
		public int Count {
			get {
				return enfermedades.Count;
			}
		}

		/// <summary>
		/// Método que devuelve el id de la última enfermedad insertada.
		/// </summary>
		/// <returns>El id del último objeto Enfermedad insertado en el diccionario de enfermedades.</returns>
		private int getLastId() {
			int toRet = 0;
			foreach (int id in enfermedades.Keys) {
				toRet = id;
			}
			return toRet;
		}

		private SortedDictionary<int,Enfermedad> enfermedades;
		private GestionBDEnfermedades bd;
		private int actualId;
	}
}