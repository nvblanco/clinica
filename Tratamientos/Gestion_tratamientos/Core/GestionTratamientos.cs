using System;
using System.Collections.Generic;
using Gestion_tratamientos.BD;

namespace Gestion_tratamientos.Core
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, borrados, modificaciones y consultas de objetos de tipo <see cref="Tratamiento"/> en memoria.
	/// </summary>
	public class GestionTratamientos
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase GestionTratamientos con los tratamientos que se pasan por parámetro.
		/// </summary>
		/// <param name="dictionary">Diccionario de objetos de tipo Tratamiento.</param>
		public GestionTratamientos(SortedDictionary<int, Tratamiento> dictionary) {
			this.tratamientos = dictionary;
			this.bd = new GestionBDTratamientos();
			actualId = getLastId() + 1;
		}

		/// <summary>
		/// Método que nserta en el diccionario de tratamientos el objeto Tratamiento pasado como parámetro.
		/// </summary>
		/// <param name="tratamiento">El objeto Tratamiento a insertar.</param>
		public void Insertar(Tratamiento tratamiento) {
			tratamiento.Id = actualId;
			tratamientos.Add(actualId,tratamiento);
			actualId++;
			bd.Insert(tratamiento);
		}

		/// <summary>
		/// Método que modifica en el diccionario de tratamientos el objeto Tratamiento pasado como parámetro.
		/// </summary>
		/// <param name="t">El objeto Tratamiento con los datos que se quieren modificar.</param>
		public void Modificar(Tratamiento t) {
			tratamientos.Remove(t.Id);
			tratamientos.Add(t.Id,t);
			bd.Update(t);
		}

		/// <summary>
		/// Método que borra del diccionario de tratamientos el tratamiento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id del tratamiento a borrar.</param>
		public void Borrar(int id) {
			tratamientos.Remove (id);
			bd.Delete(id);
		}

		/// <summary>
		/// Método que obtiene del diccionario el objeto Tratamiento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">El id del tratamiento a consultar.</param>
		public Tratamiento Obtener(int id) {
			return tratamientos [id];
		}

		/// <summary>
		/// Método que devuelve la lista de objetos de tipo Tratamiento contenidos en el diccionario de tratamientos.
		/// </summary>
		/// <returns>Lista de objetos Tratamiento presentes en el diccionario</returns>
		public List<Tratamiento> Listar() {
			return new List<Tratamiento>(tratamientos.Values);
		}

		/// <summary>
		/// Devuelve el número de objetos de tipo Tratamiento presentes en el diccionario.
		/// </summary>
		/// <value>El número de tratamientos.</value>
		public int Count {
			get {
				return tratamientos.Count;
			}
		}

		/// <summary>
		/// Método que devuelve el id del último tratamiento insertado.
		/// </summary>
		/// <returns>El id del último objeto Tratamiento insertado en el diccionario de tratamientos.</returns>
		private int getLastId() {
			int toRet = 0;
			foreach (int id in tratamientos.Keys) {
				toRet = id;
			}
			return toRet;
		}

		private SortedDictionary<int,Tratamiento> tratamientos;
		private GestionBDTratamientos bd;
		private int actualId;
	}
}