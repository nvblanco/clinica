using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa un objeto <see cref="TabPage"/> con la información de los objetos de tipo <see cref="Core.Tratamiento"/>.
	/// </summary>
	public class TratamientosTab : TabPage
	{

		public const int ColId = 0;
		public const int ColDuracion = 1;
		public const int ColMedicamentos = 2;
		public const int ColEnfermedad = 3;
		public const int ColPaciente = 4;

		/// <summary>
		/// Inicializa una nueva instancia de la clase TratamientosTab.
		/// </summary>
		/// <param name="controlador">Controlador que maneja los eventos.</param>
		public TratamientosTab (Controlador controlador) {
			this.controlador = controlador;
			this.Build ();
		}

		private void Build() {
			this.Text = "Tratamientos";

			var splitContainer = new SplitContainer ();
			splitContainer.SplitterDistance = 110;
			splitContainer.Dock = DockStyle.Fill;
			splitContainer.Orientation = Orientation.Vertical;
			this.Controls.Add (splitContainer);

			//SplitterPanel Izquierda
			var panelIzquierda = new SplitterPanel (splitContainer);
			panelIzquierda.Dock = DockStyle.Fill;
			panelIzquierda.Padding = new Padding(10);

			//Tabla para mostrar datos
			BuildGridTratamientos ();
			this.grdListaTratamientos.Dock = DockStyle.Fill;
			panelIzquierda.Controls.Add (this.grdListaTratamientos);

			//SplitterPanel Derecha
			var splitContainerDer = new SplitContainer ();
			splitContainerDer.Dock = DockStyle.Fill;
			splitContainerDer.Padding = new Padding (10);
			splitContainerDer.Orientation = Orientation.Horizontal;

			var splitArriba = new SplitterPanel (splitContainerDer);
			splitArriba.Dock = DockStyle.Fill;

			var tableBuscar = new TableLayoutPanel ();
			tableBuscar.Dock = DockStyle.Fill;
			tableBuscar.Padding = new Padding (10);
			tableBuscar.RowCount = 3;

			//Label buscar por:
			var lblBuscarPor = new Label ();
			lblBuscarPor.Text = "Buscar por:";
			tableBuscar.Controls.Add (lblBuscarPor);

			//ComboBox elegir tipo busqueda
			var comboBox = new ComboBox ();
			comboBox.Dock = DockStyle.Fill;
			comboBox.FlatStyle = FlatStyle.Flat;
			comboBox.Items.Add ("Paciente");
			comboBox.Items.Add ("Enfermedad");
			comboBox.Items.Add ("Medicamento");
			comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			comboBox.SelectedIndex =0;
			tableBuscar.Controls.Add (comboBox);

			//Textbox cadena de busqueda
			var tbSearch = new TextBox ();

			tbSearch.BorderStyle = BorderStyle.FixedSingle;
			tbSearch.Dock = DockStyle.Fill;
			tbSearch.TextChanged += (sender, e) => this.RealizarBusquedaTratamientos(tbSearch.Text,comboBox.SelectedItem.ToString());
			tableBuscar.Controls.Add (tbSearch);

			splitArriba.Controls.Add (tableBuscar);

			var splitAbajo = new SplitterPanel (splitContainerDer);
			var tableButInsModBor = new TableLayoutPanel ();
			tableButInsModBor.RowCount = 3;
			tableButInsModBor.Dock = DockStyle.Fill;

			//Boton insertar
			var btInsert = new Button ();
			btInsert.Dock = DockStyle.Fill;
			btInsert.FlatStyle = FlatStyle.Flat;
			btInsert.Text = "Insertar";
			btInsert.Click += (sender, e) => {
				if(controlador.NumMedicamentos==0 ||
				   controlador.NumEnfermedades==0 ||
				   controlador.NumPacientes==0)
				{
					MessageBox.Show("Deben existir por lo menos un paciente, un medicamento y una enfermedad",
					                "Error",MessageBoxButtons.OK);
				}
				else{
					this.InsertarTratamiento ();
					this.ActualizaTratamientos ();
				}
			};

			//Boton modificar
			var btMod = new Button ();
			btMod.Dock = DockStyle.Fill;
			btMod.FlatStyle = FlatStyle.Flat;
			btMod.Text ="Modificar";
			btMod.Click += (sender, e) => {
				int idTratamientoSeleccionado = Convert.ToInt32 (this.grdListaTratamientos.CurrentRow.Cells[0].Value);
				if(idTratamientoSeleccionado==-1){
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				}
				else{
					this.ModificarTratamiento (idTratamientoSeleccionado);
					this.ActualizaTratamientos();
				}
			};

			//Boton borrar
			var btBor = new Button ();
			btBor.Dock = DockStyle.Top;
			btBor.FlatStyle = FlatStyle.Flat;
			btBor.Text ="Borrar";
			btBor.Click += (sender, e) => {
				int idTratamientoSeleccionado = Convert.ToInt32 (this.grdListaTratamientos.CurrentRow.Cells[0].Value);
				if(idTratamientoSeleccionado==-1){
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				}
				else{
					this.BorrarTratamiento (idTratamientoSeleccionado);
					ActualizaTratamientos ();
				}
			};

			tableButInsModBor.Controls.Add (btInsert);
			tableButInsModBor.Controls.Add (btMod);
			tableButInsModBor.Controls.Add (btBor);

			splitAbajo.Controls.Add (tableButInsModBor);

			splitContainerDer.Panel1.Controls.Add (splitArriba);
			splitContainerDer.Panel2.Controls.Add (splitAbajo);
			splitContainer.Panel1.Controls.Add(panelIzquierda);
			splitContainer.Panel2.Controls.Add (splitContainerDer);

		}

		private void RealizarBusquedaTratamientos (string cadena, string filtroBusqueda) {
			switch (filtroBusqueda) {
				case "Paciente":
				BusquedaTratamientoPorPaciente (cadena);
				break;
				case "Enfermedad":
				BusquedaTratamientoPorEnfermedad (cadena);
				break;
				case "Medicamento":
				BusquedaTratamientoPorMedicamento (cadena);
				break;
				default:
				break;
			}
		}

		private void BusquedaTratamientoPorPaciente(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaTratamientos ();
			} else {
				List<Tratamiento> trat = new List<Tratamiento> ();
				foreach (Tratamiento t in controlador.ListarTratamientos()) {
					string nomApellidos = controlador.ObtenerPaciente(t.Paciente.Id).Nombre + " " +
						controlador.ObtenerPaciente (t.Paciente.Id).Apellidos;
					if (nomApellidos.ToLower ().Contains (cadena.ToLower ())) {
						trat.Add (t);
					}
				}
				this.MostrarListaTratamientosEnGrid (trat);
			}
		}

		private void BusquedaTratamientoPorEnfermedad(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaTratamientos ();
			} else {
				List<Tratamiento> trat = new List<Tratamiento> ();
				foreach (Tratamiento t in controlador.ListarTratamientos()) {
					string enfermedad = controlador.ObtenerEnfermedad(t.Enfermedad.Id).Nombre;
					if (enfermedad.ToLower ().Contains (cadena.ToLower ())) {
						trat.Add (t);
					}
				}
				this.MostrarListaTratamientosEnGrid (trat);
			}
		}

		private void BusquedaTratamientoPorMedicamento(string cadena) {
			if(cadena.Equals("")){
				this.ActualizaTratamientos();
			} else {
				List<Tratamiento> trat = new List<Tratamiento>();
				foreach(Tratamiento t in controlador.ListarTratamientos()){
					string medicamentos = "";
					foreach (Medicamento m in t.ListaMedicamentos) {
						medicamentos += controlador.ObtenerMedicamento (m.Id).Nombre + " ";
					}
					if(medicamentos.ToLower().Contains(cadena.ToLower())){
						trat.Add(t);
					}
				}
				this.MostrarListaTratamientosEnGrid (trat);
			}
		}

		private void MostrarListaTratamientosEnGrid(List<Tratamiento> list) {
			this.grdListaTratamientos.Rows.Clear ();
			for (int i = 0; i < list.Count; i++) {

				this.grdListaTratamientos.Rows.Add();

				DataGridViewRow row = this.grdListaTratamientos.Rows[ i ];
				Tratamiento tratamiento = list[i];

				row.Cells[ ColId ].Value = tratamiento.Id.ToString().PadLeft( 4, ' ' );
				row.Cells[ ColDuracion ].Value = tratamiento.Duracion+" días";
				string medicamentos = "";
				foreach (Medicamento m in tratamiento.ListaMedicamentos){
					medicamentos += controlador.ObtenerMedicamento (m.Id).Nombre+", ";
				}
				medicamentos=medicamentos.Remove (medicamentos.Length - 2); //Borrar coma do final
				row.Cells [ColMedicamentos].Value = medicamentos;
				row.Cells[ ColEnfermedad ].Value = controlador.ObtenerEnfermedad(tratamiento.Enfermedad.Id).Nombre;
				row.Cells [ColPaciente].Value = controlador.ObtenerPaciente (tratamiento.Paciente.Id).Nombre +" "+
					controlador.ObtenerPaciente (tratamiento.Paciente.Id).Apellidos;

			}
		}

		private void InsertarTratamiento() {
			this.insModTratamiento = new InsModTratamientoWindow (-1, controlador);
			this.insModTratamiento.Build ();
			this.insModTratamiento.Show ();
			this.insModTratamiento.FormClosed += (sender, e) => ActualizaTratamientos ();
		}

		private void ModificarTratamiento(int id) {
			this.insModTratamiento = new InsModTratamientoWindow (id, controlador);
			this.insModTratamiento.Build ();
			this.insModTratamiento.Show ();
			this.insModTratamiento.FormClosed += (sender, e) => ActualizaTratamientos ();
		}

		private void BorrarTratamiento(int id) {
			controlador.BorrarTratamiento (id);
		}



		private void ActualizaFilaDeListaTratamientos(int rowIndex, Tratamiento tratamiento) {

			DataGridViewRow row = this.grdListaTratamientos.Rows[ rowIndex ];

			row.Cells[ ColId ].Value = tratamiento.Id.ToString().PadLeft( 4, ' ' );
			row.Cells[ ColDuracion ].Value = tratamiento.Duracion+" días";
			row.Cells[ ColEnfermedad ].Value = controlador.ObtenerEnfermedad(tratamiento.Enfermedad.Id).Nombre;
			string medicamentos = "";
			foreach (Medicamento m in tratamiento.ListaMedicamentos){
				medicamentos += controlador.ObtenerMedicamento (m.Id).Nombre+", ";
			}
			if (medicamentos.Length > 1) {
				medicamentos = medicamentos.Remove (medicamentos.Length - 2); //Borrar coma do final
			}
			row.Cells [ColMedicamentos].Value = medicamentos;
			row.Cells [ColPaciente].Value = controlador.ObtenerPaciente (tratamiento.Paciente.Id).Nombre +" "+
				controlador.ObtenerPaciente (tratamiento.Paciente.Id).Apellidos;

			return;
		}

		/// <summary>
		/// Método que actualiza el grid en el que se muestran los objetos de tipo Core.Tratamiento.
		/// </summary>
		public void ActualizaTratamientos() {
			int i = 0;
			List<Tratamiento> lista = controlador.ListarTratamientos ();
			BorrarLista ();
			foreach (Tratamiento tratamiento in lista) {
				this.grdListaTratamientos.Rows.Add ();
				ActualizaFilaDeListaTratamientos (i++, tratamiento);
			}
			return;
		}

		private void BorrarLista () {
			this.grdListaTratamientos.Rows.Clear ();
			this.grdListaTratamientos.Refresh ();
		}

		private void BuildGridTratamientos() {

			var grdLista = new DataGridView();
			grdLista.Dock = DockStyle.Fill;
			grdLista.AllowUserToResizeRows = false;
			grdLista.RowHeadersVisible = false;
			grdLista.AutoGenerateColumns = false;
			grdLista.MultiSelect = false;
			grdLista.AllowUserToAddRows = false;
			var textCellTemplate0 = new DataGridViewTextBoxCell();
			var textCellTemplate1 = new DataGridViewTextBoxCell();
			var textCellTemplate2 = new DataGridViewTextBoxCell();
			var textCellTemplate3 = new DataGridViewTextBoxCell();
			var textCellTemplate4 = new DataGridViewTextBoxCell();
			textCellTemplate0.Style.BackColor = grdLista.RowHeadersDefaultCellStyle.BackColor;
			textCellTemplate1.Style.BackColor = Color.Lavender;
			textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate2.Style.BackColor = Color.Lavender;
			textCellTemplate2.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate3.Style.BackColor = Color.Lavender;
			textCellTemplate3.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate4.Style.BackColor = Color.Lavender;
			textCellTemplate4.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			var column0 = new DataGridViewTextBoxColumn();
			var column1 = new DataGridViewTextBoxColumn();
			var column2 = new DataGridViewTextBoxColumn();
			var column3 = new DataGridViewTextBoxColumn();
			var column4 = new DataGridViewTextBoxColumn();
			column0.SortMode = DataGridViewColumnSortMode.NotSortable;
			column1.SortMode = DataGridViewColumnSortMode.NotSortable;
			column2.SortMode = DataGridViewColumnSortMode.NotSortable;
			column3.SortMode = DataGridViewColumnSortMode.NotSortable;
			column4.SortMode = DataGridViewColumnSortMode.NotSortable;
			column0.CellTemplate = textCellTemplate0;
			column1.CellTemplate = textCellTemplate1;
			column2.CellTemplate = textCellTemplate2;
			column3.CellTemplate = textCellTemplate3;
			column4.CellTemplate = textCellTemplate4;
			column0.HeaderText = "#";
			column0.Width = 50;
			column0.ReadOnly = true;
			column1.HeaderText = "Duracion";
			column1.Width = 70;
			column1.ReadOnly = true;
			column2.HeaderText = "Medicamentos";
			column2.Width = 202;
			column2.ReadOnly = true;
			column3.HeaderText = "Enfermedad";
			column3.Width = 100;
			column3.ReadOnly = true;
			column4.HeaderText = "Paciente";
			column4.Width = 170;
			column4.ReadOnly = true;

			grdLista.Columns.AddRange( new DataGridViewColumn[] {
				column0, column1, column2, column3, column4
			} );

			this.grdListaTratamientos = grdLista;
			ActualizaTratamientos ();

		}
			
		private Controlador controlador;
		private DataGridView grdListaTratamientos;
		private InsModTratamientoWindow insModTratamiento;
	}
}