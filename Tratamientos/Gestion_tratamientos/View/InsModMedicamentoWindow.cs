using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using Gestion_tratamientos.Utils;
using System.Text;
using Gestion_tratamientos;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa la ventana en la que se insertan o modifican objetos de tipo <see cref="Core.Medicamento"/>s.
	/// </summary>
	public class InsModMedicamentoWindow : Form
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase InsModMedicamentoWindow.
		/// </summary>
		/// <param name="id">Id del objeto Medicamento a modificar (-1 cuando se inserta).</param>
		/// <param name="controller">Controlador que inserta/modifica el objeto en el modelo.</param>
		public InsModMedicamentoWindow (int id,Controlador controller) {
			this.id = id;
			this.controlador = controller;
		}

		/// <summary>
		/// Método que construye la ventana en la que el usuario introduce los datos.
		/// </summary>
		public void Build() {

			if (id == -1) {
				this.Text = "Insertar nuevo medicamento";
				this.modificando = false;
			} else {
				this.modificando = true;
				this.Text = "Modificar medicamento #"+id.ToString();
				this.medicamento = controlador.ObtenerMedicamento (id);
			}
			this.insModPanel = new TableLayoutPanel();
			this.insModPanel.Padding = new Padding (10);
			this.insModPanel.RowCount = 5;
			this.insModPanel.Dock = DockStyle.Fill;

			this.medicamentoPanel = new Panel ();
			this.medicamentoPanel.Height = 20;
			this.medicamentoPanel.Dock = DockStyle.Fill;
			this.medicamentoLabel = new Label ();
			this.medicamentoLabel.Text = "Medicamento:";
			this.medicamentoLabel.Dock = DockStyle.Left;
			this.medicamentoPanel.Controls.Add (medicamentoLabel);

			this.medicamentoTextBox = new TextBox ();
			if (modificando) {
				this.medicamentoTextBox.Text = this.medicamento.Nombre;
			}
			this.medicamentoTextBox.Dock = DockStyle.Right;
			this.medicamentoTextBox.Width = 200;
			this.medicamentoPanel.Controls.Add (this.medicamentoTextBox);

			//Enfermedades
			this.enfermedadesPanel = new Panel ();
			this.enfermedadesPanel.Dock = DockStyle.Fill;
			this.enfermedadesLabel = new Label ();
			this.enfermedadesLabel.Text = "Enfermedades:";
			this.enfermedadesLabel.Dock = DockStyle.Left;
			this.enfermedadesPanel.Controls.Add (enfermedadesLabel);

			this.enfermedadesCheckBox = new CheckedListBox();
			bool ischecked = false;

			foreach(Enfermedad e in controlador.ListarEnfermedades()) {

				if(modificando) {
					if(medicamento.ListaEnfermedades.Contains(e)) {
						ischecked = true;
					} else {
						ischecked = false;
					}
				}

				enfermedadesCheckBox.Items.Add(new Thing() {
					Key = e.Id.ToString(),
					Value = e.Nombre
				}, ischecked);
			}

			this.enfermedadesCheckBox.Dock = DockStyle.Right;
			this.enfermedadesCheckBox.Width = 200;
			this.enfermedadesPanel.Controls.Add (enfermedadesCheckBox);

			//Botones
			this.botonesPanel = new TableLayoutPanel();
			this.botonesPanel.Height = 30;
			this.botonesPanel.ColumnCount = 2;
			this.botonAceptar = new Button();
			this.botonAceptar.Size = new Size(80,25);
			this.botonAceptar.Text = "Aceptar";
			this.botonAceptar.Dock = DockStyle.Left;
			if (modificando) {
				this.botonAceptar.Click += (sender, e) => {

					this.ModificarMedicamento();
					this.Hide ();
					this.Close ();
				};
			} else {

				this.botonAceptar.Click += (sender, e) => {
					this.InsertarMedicamento ();
					this.Hide ();
					this.Close ();
				};
			}
			this.botonesPanel.SetColumn(botonAceptar,1);
			this.botonesPanel.Controls.Add (botonAceptar);
			this.botonCancelar = new Button ();
			this.botonCancelar.Size = new Size (80,25);
			this.botonCancelar.Text = "Cancelar";
			this.botonCancelar.Click += (sender, e) => {
				this.Hide();
				this.Close();
			};
			this.botonCancelar.Dock = DockStyle.Right;
			this.botonesPanel.SetColumn(botonCancelar,2);
			this.botonesPanel.Controls.Add (botonCancelar);

			this.insModPanel.Controls.Add (medicamentoPanel);
			this.insModPanel.Controls.Add (enfermedadesPanel);
			this.insModPanel.Controls.Add (botonesPanel);

			this.MinimumSize = new Size (400, 300);
			this.Controls.Add (insModPanel);

		}

		private void InsertarMedicamento() {
			string nombre = this.medicamentoTextBox.Text;
			var listaIdsEnfermedades = new List<int> ();

			int checkedId;
			foreach(var s in enfermedadesCheckBox.CheckedItems) {
				checkedId = Convert.ToInt32(((Thing)s).Key);
				listaIdsEnfermedades.Add(checkedId);
			}
			controlador.InsertarMedicamento(nombre, listaIdsEnfermedades);
		}

		private void ModificarMedicamento() {
			int idViejo = medicamento.Id;
			string nombre = this.medicamentoTextBox.Text;
			var listaIdsEnfermedades = new List<int> ();
			int checkedId;
			foreach(var s in enfermedadesCheckBox.CheckedItems) {
				checkedId = Convert.ToInt32(((Thing)s).Key);
				listaIdsEnfermedades.Add(checkedId);
			}
			controlador.ModificarMedicamento(idViejo, nombre, listaIdsEnfermedades);
		}

		private TableLayoutPanel insModPanel;
		private Panel medicamentoPanel;
		private Label medicamentoLabel;
		private TextBox medicamentoTextBox;
		private Panel enfermedadesPanel;
		private Label enfermedadesLabel;
		private CheckedListBox enfermedadesCheckBox;
		private TableLayoutPanel botonesPanel;
		private Button botonAceptar;
		private Button botonCancelar;
		private int id;
		private Controlador controlador;
		private bool modificando;
		private Medicamento medicamento;
	}
}