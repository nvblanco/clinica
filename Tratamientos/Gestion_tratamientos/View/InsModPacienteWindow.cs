using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa la ventana en la que se insertan o modifican objetos de tipo <see cref="Core.Paciente"/>.
	/// </summary>
	public class InsModPacienteWindow : Form
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase InsModPacienteWindow.
		/// </summary>
		/// <param name="id">Id del objeto Paciente a modificar (-1 cuando se inserta).</param>
		/// <param name="controlador">Controlador que inserta/modifica el objeto en el modelo.</param>
		public InsModPacienteWindow(int id, Controlador controlador) {
			this.id = id;
			this.controlador = controlador;
		}

		/// <summary>
		/// Método que construye la ventana en la que el usuario introduce los datos.
		/// </summary>
		public void Build() {

			if (id == -1) {
				this.Text = "Insertar nuevo paciente";
				this.modificando = false;
			} else {
				this.modificando = true;
				this.Text = "Modificar paciente #"+id.ToString();
				this.pacienteAModificar = controlador.ObtenerPaciente (id);
			}
			this.insModPanel = new TableLayoutPanel();
			this.insModPanel.Padding = new Padding (10);
			this.insModPanel.RowCount = 5;

			this.insModPanel.Dock = DockStyle.Fill;

			//Nombre
			this.nombrePanel = new Panel ();
			this.nombrePanel.Height = 20;
			this.nombrePanel.Dock = DockStyle.Fill;
			this.nombreLabel = new Label ();
			this.nombreLabel.Text = "Nombre:";
			this.nombreLabel.Dock = DockStyle.Left;
			this.nombrePanel.Controls.Add (nombreLabel);
			this.nombreTextBox = new TextBox ();
			if (modificando) {
				this.nombreTextBox.Text = pacienteAModificar.Nombre.ToString();
			}
			this.nombreTextBox.Dock = DockStyle.Right;
			this.nombreTextBox.Width = 200;
			this.nombrePanel.Controls.Add (nombreTextBox);

			//Apellidos
			this.apellidosPanel = new Panel ();
			this.apellidosPanel.Height = 20;
			this.apellidosPanel.Dock = DockStyle.Fill;
			this.apellidosLabel = new Label ();
			this.apellidosLabel.Text = "Apellidos:";
			this.apellidosLabel.Dock = DockStyle.Left;
			this.apellidosPanel.Controls.Add (apellidosLabel);
			this.apellidosTextBox = new TextBox ();
			if (modificando) {
				this.apellidosTextBox.Text = pacienteAModificar.Apellidos.ToString();
			}
			this.apellidosTextBox.Dock = DockStyle.Right;
			this.apellidosTextBox.Width = 200;
			this.apellidosPanel.Controls.Add (apellidosTextBox);

			//DNI
			this.dniPanel = new Panel ();
			this.dniPanel.Height = 20;
			this.dniPanel.Dock = DockStyle.Fill;
			this.dniLabel = new Label ();
			this.dniLabel.Text = "DNI:";
			this.dniLabel.Dock = DockStyle.Left;
			this.dniPanel.Controls.Add (dniLabel);
			this.dniTextBox = new TextBox ();
			if (modificando) {
				this.dniTextBox.Text = pacienteAModificar.DNI.ToString();
			}
			this.dniTextBox.Dock = DockStyle.Right;
			this.dniTextBox.Width = 200;
			this.dniPanel.Controls.Add (dniTextBox);

			//Fecha nacimiento
			this.fechaNacPanel = new Panel ();
			this.fechaNacPanel.Height = 20;
			this.fechaNacPanel.Dock = DockStyle.Fill;
			this.fechaNacLabel = new Label ();
			this.fechaNacLabel.Text = "Fecha de nacimiento:";
			this.fechaNacLabel.Dock = DockStyle.Left;
			this.fechaNacPanel.Controls.Add (fechaNacLabel);

			// Create a new DateTimePicker control and initialize it.
			this.dateTimePicker = new DateTimePicker();
			dateTimePicker.MinDate = new DateTime(1900, 1, 1);
			dateTimePicker.MaxDate = DateTime.Today;
			if (modificando) {
				var fechaArray = pacienteAModificar.FechaNac.Split ('/');
				int dia = Convert.ToInt32(fechaArray [0]);
				int mes = Convert.ToInt32(fechaArray [1]);
				int ano = Convert.ToInt32(fechaArray [2]);
				DateTime dt = new DateTime (ano,mes,dia);
				dateTimePicker.Checked = true;
				dateTimePicker.Value = dt;
			}
			// Set the MinDate and MaxDate.


			// Set the CustomFormat string.
			dateTimePicker.CustomFormat = "dd/MM/yyyy";
			dateTimePicker.Format = DateTimePickerFormat.Custom;

			// Show the CheckBox and display the control as an up-down control.
			dateTimePicker.ShowCheckBox = true;
			dateTimePicker.ShowUpDown = false;

			dateTimePicker.Dock = DockStyle.Right;
			this.fechaNacPanel.Controls.Add (dateTimePicker);

			//Botones
			this.botonesPanel = new TableLayoutPanel ();
			this.botonesPanel.Height = 30;
			this.botonesPanel.ColumnCount = 2;
			this.botonAceptar = new Button ();
			this.botonAceptar.Size = new Size (80,25);
			this.botonAceptar.Text = "Aceptar";
			this.botonAceptar.Dock = DockStyle.Left;
			if (modificando) {
				this.botonAceptar.Click += (sender, e) => {
					this.ModificarPaciente ();
					this.Hide ();
					this.Close ();
				};
			} else {
				this.botonAceptar.Click += (sender, e) => {
					this.InsertarPaciente ();
					this.Hide ();
					this.Close ();
				};
			}
			this.botonesPanel.SetColumn(botonAceptar,1);
			this.botonesPanel.Controls.Add (botonAceptar);
			this.botonCancelar = new Button ();
			this.botonCancelar.Size = new Size (80,25);
			this.botonCancelar.Text = "Cancelar";
			this.botonCancelar.Click += (sender, e) => {
				this.Hide ();
				this.Close ();
			};
			this.botonCancelar.Dock = DockStyle.Right;
			this.botonesPanel.SetColumn(botonCancelar,2);
			this.botonesPanel.Controls.Add (botonCancelar);

			this.insModPanel.Controls.Add (nombrePanel);
			this.insModPanel.Controls.Add (apellidosPanel);
			this.insModPanel.Controls.Add (dniPanel);
			this.insModPanel.Controls.Add (fechaNacPanel);
			this.insModPanel.Controls.Add (botonesPanel);

			this.MinimumSize = new Size (400, 300);
			this.Controls.Add (insModPanel);
		}

		private void InsertarPaciente() {
			int id = controlador.NumPacientes + 1 ;
			string nombre = nombreTextBox.Text;
			string apellidos = apellidosTextBox.Text;
			string dni = dniTextBox.Text;
			string fechaNac = dateTimePicker.Text;
			Paciente p = new Paciente (id, nombre, apellidos, dni, fechaNac);
			controlador.InsertarPaciente (p);
		}

		private void ModificarPaciente() {
			int idViejo = pacienteAModificar.Id;
			string nombre = nombreTextBox.Text;
			string apellidos = apellidosTextBox.Text;
			string dni = dniTextBox.Text;
			string fechaNac = dateTimePicker.Text;
			Paciente p = new Paciente (id, nombre, apellidos, dni, fechaNac);
			controlador.ModificarPaciente (idViejo, p);
		}

		private TableLayoutPanel insModPanel;
		private Panel nombrePanel;
		private Label nombreLabel;
		private TextBox nombreTextBox;
		private Panel apellidosPanel;
		private Label apellidosLabel;
		private TextBox apellidosTextBox;
		private Panel dniPanel;
		private Label dniLabel;
		private TextBox dniTextBox;
		private Panel fechaNacPanel;
		private Label fechaNacLabel;
		private DateTimePicker dateTimePicker;

		private TableLayoutPanel botonesPanel;
		private Button botonAceptar;
		private Button botonCancelar;

		private int id;
		private Controlador controlador;

		private bool modificando;
		private Paciente pacienteAModificar;

	}
}

