using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa un objeto <see cref="TabPage"/> con la información de los objetos de tipo <see cref="Core.Paciente"/>.
	/// </summary>
	public class PacientesTab : TabPage
	{
		public const int ColId = 0;
		public const int ColNombre = 1;
		public const int ColApellidos = 2;
		public const int ColDNI = 3;
		public const int ColFechaNac = 4;

		/// <summary>
		/// Inicializa una nueva instancia de la clase PacientesTab.
		/// </summary>
		/// <param name="controlador">Controlador que maneja los eventos.</param>
		public PacientesTab (Controlador controlador) {
			this.controlador = controlador;
			this.Build ();
		}

		private void Build() {
			this.Text = "Pacientes";

			var splitContainer = new SplitContainer ();
			splitContainer.SplitterDistance = 110;
			splitContainer.Dock = DockStyle.Fill;
			splitContainer.Orientation = Orientation.Vertical;
			this.Controls.Add (splitContainer);

			//SplitterPanel Izquierda
			var panelIzquierda = new SplitterPanel (splitContainer);
			panelIzquierda.Dock = DockStyle.Fill;
			panelIzquierda.Padding = new Padding(10);

			//Tabla para mostrar datos
			BuildGridPacientes ();
			this.grdListaPacientes.Dock = DockStyle.Fill;
			panelIzquierda.Controls.Add (this.grdListaPacientes);

			//SplitterPanel Derecha
			var splitContainerDer = new SplitContainer ();
			splitContainerDer.Dock = DockStyle.Fill;
			splitContainerDer.Padding = new Padding (10);
			splitContainerDer.Orientation = Orientation.Horizontal;

			var splitArriba = new SplitterPanel (splitContainerDer);
			splitArriba.Dock = DockStyle.Fill;

			var tableBuscar = new TableLayoutPanel ();
			tableBuscar.Dock=DockStyle.Fill;
			tableBuscar.Padding = new Padding (10);
			tableBuscar.RowCount = 3;

			//Label buscar por:
			var lblBuscarPor = new Label ();
			lblBuscarPor.Text = "Buscar por:";
			tableBuscar.Controls.Add (lblBuscarPor);

			//ComboBox elegir tipo busqueda
			var comboBox = new ComboBox ();
			comboBox.Dock = DockStyle.Fill;
			comboBox.FlatStyle = FlatStyle.Flat;
			comboBox.Items.Add ("Nombre");
			comboBox.Items.Add ("Apellidos");
			comboBox.Items.Add ("DNI");
			comboBox.Items.Add ("Fecha de nacimiento");
			comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			comboBox.SelectedIndex =0;
			tableBuscar.Controls.Add (comboBox);

			//Textbox cadena de busqueda
			var tbSearch = new TextBox ();
			tbSearch.BorderStyle = BorderStyle.FixedSingle;
			tbSearch.Dock = DockStyle.Fill;
			tbSearch.TextChanged += (sender, e) => this.RealizarBusquedaPacientes(tbSearch.Text,comboBox.SelectedItem.ToString());
			tableBuscar.Controls.Add (tbSearch);

			splitArriba.Controls.Add (tableBuscar);

			var splitAbajo = new SplitterPanel (splitContainerDer);
			var tableButInsModBor = new TableLayoutPanel ();
			tableButInsModBor.RowCount = 3;
			tableButInsModBor.Dock = DockStyle.Fill;

			//Boton insertar
			var btInsert = new Button ();
			btInsert.Text = "Insertar";
			btInsert.Dock = DockStyle.Fill;
			btInsert.FlatStyle = FlatStyle.Flat;
			btInsert.Click += (sender, e) => {
				this.InsertarPaciente ();
				this.ActualizaPacientes ();
			};

			//Boton modificar
			var btMod = new Button ();
			btMod.Dock = DockStyle.Fill;
			btMod.Text ="Modificar";
			btMod.FlatStyle = FlatStyle.Flat;
			btMod.Click += (sender, e) => {
				if(this.GetIdPacienteSeleccionado()==-1){
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				}
				else{
					this.ModificarPaciente (this.GetIdPacienteSeleccionado ());
					this.ActualizaPacientes ();
				}
			};

			//Boton borrar
			var btBor = new Button ();
			btBor.Dock = DockStyle.Top;
			btBor.Text ="Borrar";
			btBor.FlatStyle = FlatStyle.Flat;
			btBor.Click += (sender, e) => {
				if(this.GetIdPacienteSeleccionado()==-1){
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				}
				else{
					this.BorrarPaciente (this.GetIdPacienteSeleccionado ());
				}
			};

			tableButInsModBor.Controls.Add (btInsert);
			tableButInsModBor.Controls.Add (btMod);
			tableButInsModBor.Controls.Add (btBor);

			splitAbajo.Controls.Add (tableButInsModBor);

			splitContainerDer.Panel1.Controls.Add (splitArriba);
			splitContainerDer.Panel2.Controls.Add (splitAbajo);
			splitContainer.Panel1.Controls.Add(panelIzquierda);
			splitContainer.Panel2.Controls.Add (splitContainerDer);
		}

		private void RealizarBusquedaPacientes (string cadena, string filtroBusqueda) {
			switch (filtroBusqueda) {
				case "Nombre":
					BusquedaPacientePorNombre (cadena);
					break;
				case "Apellidos":
					BusquedaPacientePorApellidos (cadena);
					break;
				case "DNI":
					BusquedaPacientePorDNI (cadena);
					break;
				case "Fecha de nacimiento":
					BusquedaPacientePorFechaNac (cadena);
					break;
				default:
					break;
			}
		}

		private void BusquedaPacientePorNombre(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaPacientes ();
			} else {
				List<Paciente> pac = new List<Paciente> ();
				foreach (Paciente p in controlador.ListarPacientes()) {
					if (p.Nombre.ToLower ().Contains (cadena.ToLower ())) {
						pac.Add (p);
					}
				}
				this.MostrarListaPacientesEnGrid (pac);
			}
		}

		private void BusquedaPacientePorApellidos(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaPacientes ();
			} else {
				List<Paciente> pac = new List<Paciente> ();
				foreach (Paciente p in controlador.ListarPacientes()) {
					if (p.Apellidos.ToLower ().Contains (cadena.ToLower ())) {
						pac.Add (p);
					}
				}
				this.MostrarListaPacientesEnGrid (pac);
			}
		}

		private void BusquedaPacientePorDNI(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaPacientes ();
			} else {
				List<Paciente> pac = new List<Paciente> ();
				foreach (Paciente p in controlador.ListarPacientes()) {
					if (p.DNI.ToLower ().Contains (cadena.ToLower ())) {
						pac.Add (p);
					}
				}
				this.MostrarListaPacientesEnGrid (pac);
			}
		}

		private void BusquedaPacientePorFechaNac(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaPacientes ();
			} else {
				List<Paciente> pac = new List<Paciente> ();
				foreach (Paciente p in controlador.ListarPacientes()) {
					if (p.FechaNac.ToLower ().Contains (cadena.ToLower ())) {
						pac.Add (p);
					}
				}
				this.MostrarListaPacientesEnGrid (pac);
			}
		}


		private void MostrarListaPacientesEnGrid(List<Paciente> list) {
			this.grdListaPacientes.Rows.Clear ();
			for (int i = 0; i < list.Count; i++) {
				if ( this.grdListaPacientes.Rows.Count <= i ) {
					this.grdListaPacientes.Rows.Add();
				}
			
				DataGridViewRow row = this.grdListaPacientes.Rows[ i ];
				Paciente paciente = list[i];

				row.Cells[ ColId ].Value = paciente.Id.ToString().PadLeft( 4, ' ' );
				row.Cells[ ColNombre ].Value = paciente.Nombre;
				row.Cells[ ColApellidos ].Value = paciente.Apellidos;
				row.Cells[ ColDNI ].Value = paciente.DNI;
				row.Cells[ ColFechaNac ].Value = paciente.FechaNac;
			}
		}

		private void InsertarPaciente() {
			this.insModPaciente = new InsModPacienteWindow (-1, controlador);
			this.insModPaciente.Build ();
			this.insModPaciente.Show ();
			this.insModPaciente.FormClosed += (sender, e) => ActualizaPacientes ();
		}

		private void ModificarPaciente(int id) {
			this.insModPaciente = new InsModPacienteWindow (id, controlador);
			this.insModPaciente.Build ();
			this.insModPaciente.Show ();
			this.insModPaciente.FormClosed += (sender, e) => ActualizaPacientes ();
		}

		private void BorrarPaciente(int id) {
			controlador.BorrarPaciente (id);
			ActualizaPacientes ();
		}

		private int GetIdPacienteSeleccionado() {
			try{
				int actual_row = this.grdListaPacientes.CurrentRow.Index;
				return Convert.ToInt32 (this.grdListaPacientes.Rows [actual_row].Cells [0].Value);
			}
			catch (NullReferenceException){
				return -1;
			}
		}

		private void ActualizaFilaDeListaPacientes(int rowIndex, Paciente p) {
			DataGridViewRow row = this.grdListaPacientes.Rows[ rowIndex ];

			row.Cells[ ColId ].Value = p.Id.ToString().PadLeft( 4, ' ' );
			row.Cells[ ColNombre ].Value = p.Nombre;
			row.Cells[ ColApellidos ].Value = p.Apellidos;
			row.Cells[ ColDNI ].Value = p.DNI;
			row.Cells[ ColFechaNac ].Value = p.FechaNac;

			return;
		}

		/// <summary>
		/// Método que actualiza el grid en el que se muestran los objetos de tipo Core.Paciente.
		/// </summary>
		public void ActualizaPacientes() {
			int i = 0;
			List<Paciente> lista = controlador.ListarPacientes ();
			BorrarLista ();
			foreach (Paciente paciente in lista) {
				this.grdListaPacientes.Rows.Add ();
				ActualizaFilaDeListaPacientes (i++, paciente);
			}
		
		}

		private void BorrarLista() {
			this.grdListaPacientes.Rows.Clear ();
			this.grdListaPacientes.Refresh ();
		}

		private void BuildGridPacientes() {
			var grdLista = new DataGridView();
			grdLista.Dock = DockStyle.Fill;
			grdLista.AllowUserToResizeRows = false;
			grdLista.RowHeadersVisible = false;
			grdLista.AutoGenerateColumns = false;
			grdLista.MultiSelect = false;
			grdLista.AllowUserToAddRows = false;
			var textCellTemplate0 = new DataGridViewTextBoxCell();
			var textCellTemplate1 = new DataGridViewTextBoxCell();
			var textCellTemplate2 = new DataGridViewTextBoxCell();
			var textCellTemplate3 = new DataGridViewTextBoxCell();
			var textCellTemplate4 = new DataGridViewTextBoxCell();
			textCellTemplate0.Style.BackColor = grdLista.RowHeadersDefaultCellStyle.BackColor;
			textCellTemplate1.Style.BackColor = Color.Lavender;
			textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate2.Style.BackColor = Color.Lavender;
			textCellTemplate2.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate3.Style.BackColor = Color.Lavender;
			textCellTemplate3.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate4.Style.BackColor = Color.Lavender;
			textCellTemplate4.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			var column0 = new DataGridViewTextBoxColumn();
			var column1 = new DataGridViewTextBoxColumn();
			var column2 = new DataGridViewTextBoxColumn();
			var column3 = new DataGridViewTextBoxColumn();
			var column4 = new DataGridViewTextBoxColumn();
			column0.SortMode = DataGridViewColumnSortMode.NotSortable;
			column1.SortMode = DataGridViewColumnSortMode.NotSortable;
			column2.SortMode = DataGridViewColumnSortMode.NotSortable;
			column3.SortMode = DataGridViewColumnSortMode.NotSortable;
			column4.SortMode = DataGridViewColumnSortMode.NotSortable;
			column0.CellTemplate = textCellTemplate0;
			column1.CellTemplate = textCellTemplate1;
			column2.CellTemplate = textCellTemplate2;
			column3.CellTemplate = textCellTemplate2;
			column4.CellTemplate = textCellTemplate2;
			column0.HeaderText = "#";
			column0.Width = 50;
			column0.ReadOnly = true;
			column1.HeaderText = "Nombre";
			column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column1.ReadOnly = true;
			column2.HeaderText = "Apellidos";
			column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column2.ReadOnly = true;
			column3.HeaderText = "DNI";
			column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column3.ReadOnly = true;
			column4.HeaderText = "FechaNac";
			column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column4.ReadOnly = true;

			grdLista.Columns.AddRange( new DataGridViewColumn[] {
				column0, column1, column2, column3, column4
			} );

			this.grdListaPacientes = grdLista;
			ActualizaPacientes ();

		}

		private Controlador controlador;
		private DataGridView grdListaPacientes;
		private InsModPacienteWindow insModPaciente;
	}
}