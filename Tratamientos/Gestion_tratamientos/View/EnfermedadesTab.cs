using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa un objeto <see cref="TabPage"/> con la información de los objetos de tipo <see cref="Core.Enfermedad"/>.
	/// </summary>
	public class EnfermedadesTab : TabPage
	{

		public const int ColId = 0;
		public const int ColNombre = 1;
		public const int ColTipo = 2;

		/// <summary>
		/// Inicializa una nueva instancia de la clase EnfermedadesTab.
		/// </summary>
		/// <param name="controlador">Controlador que maneja los eventos.</param>
		public EnfermedadesTab (Controlador controlador) {
			this.controlador = controlador;
			this.Build ();
		}

		private void Build() {

			this.Text = "Enfermedades";

			var splitContainer = new SplitContainer();
			splitContainer.SplitterDistance = 110;
			splitContainer.Dock = DockStyle.Fill;
			splitContainer.Orientation = Orientation.Vertical;
			this.Controls.Add (splitContainer);

			//SplitterPanel Izquierda
			var panelIzquierda = new SplitterPanel(splitContainer);
			panelIzquierda.Dock = DockStyle.Fill;
			panelIzquierda.Padding = new Padding(10);

			//Tabla para mostrar datos
			BuildGridEnfermedades ();
			this.grdListaEnfermedades.Dock = DockStyle.Fill;
			panelIzquierda.Controls.Add(this.grdListaEnfermedades);

			//SplitterPanel Derecha
			var splitContainerDer = new SplitContainer();
			splitContainerDer.Dock = DockStyle.Fill;
			splitContainerDer.Padding = new Padding (10);
			splitContainerDer.Orientation = Orientation.Horizontal;

			var splitArriba = new SplitterPanel(splitContainerDer);
			splitArriba.Dock = DockStyle.Fill;

			var tableBuscar = new TableLayoutPanel();
			tableBuscar.Dock = DockStyle.Fill;
			tableBuscar.Padding = new Padding (10);
			tableBuscar.RowCount = 3;

			//Label buscar por:
			var lblBuscarPor = new Label();
			lblBuscarPor.Text = "Buscar por:";
			tableBuscar.Controls.Add(lblBuscarPor);

			//ComboBox elegir tipo busqueda
			var comboBox = new ComboBox();
			comboBox.Dock = DockStyle.Fill;
			comboBox.FlatStyle = FlatStyle.Flat;
			comboBox.Items.Add("Nombre");
			comboBox.Items.Add("Tipo");
			comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			comboBox.SelectedIndex =0;
			tableBuscar.Controls.Add(comboBox);

			//Textbox cadena de busqueda
			var tbSearch = new TextBox();
			tbSearch.Width = 100;
			tbSearch.BorderStyle = BorderStyle.FixedSingle;
			tbSearch.Padding = new Padding(10);
			tbSearch.Dock = DockStyle.Fill;
			tbSearch.TextChanged += (sender, e) => this.RealizarBusquedaEnfermedades(tbSearch.Text,comboBox.SelectedItem.ToString());
			tableBuscar.Controls.Add(tbSearch);

			splitArriba.Controls.Add (tableBuscar);

			var splitAbajo = new SplitterPanel(splitContainerDer);
			var tableButInsModBor = new TableLayoutPanel ();
			tableButInsModBor.RowCount = 3;
			tableButInsModBor.Dock = DockStyle.Fill;

			//Boton insertar
			var btInsert = new Button();
			btInsert.Dock = DockStyle.Fill;
			btInsert.Text = "Insertar";
			btInsert.FlatStyle = FlatStyle.Flat;
			btInsert.Click += (sender, e) => {
				this.InsertarEnfermedad();
				this.ActualizaEnfermedades();
			};

			//Boton modificar
			var btMod = new Button();
			btMod.Dock = DockStyle.Fill;
			btMod.Text ="Modificar";
			btMod.FlatStyle = FlatStyle.Flat;
			btMod.Click += (sender, e) => {
				int idEnfermedadSeleccionada = Convert.ToInt32 (this.grdListaEnfermedades.CurrentRow.Cells[0].Value);
				if(idEnfermedadSeleccionada==-1) {
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				} else {
					this.ModificarEnfermedad (idEnfermedadSeleccionada);
					this.ActualizaEnfermedades();
				}
			};

			//Boton borrar
			var btBor = new Button ();
			btBor.Dock = DockStyle.Top;
			btBor.Text ="Borrar";
			btBor.FlatStyle = FlatStyle.Flat;
			btBor.Click += (sender, e) => {
				int idEnfermedadSeleccionada = Convert.ToInt32 (this.grdListaEnfermedades.CurrentRow.Cells[0].Value);
				if(idEnfermedadSeleccionada==-1) {
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				} else {
					this.BorrarEnfermedad (idEnfermedadSeleccionada);
					ActualizaEnfermedades ();
				}
			};

			tableButInsModBor.Controls.Add (btInsert);
			tableButInsModBor.Controls.Add (btMod);
			tableButInsModBor.Controls.Add (btBor);

			splitAbajo.Controls.Add (tableButInsModBor);

			splitContainerDer.Panel1.Controls.Add (splitArriba);
			splitContainerDer.Panel2.Controls.Add (splitAbajo);
			splitContainer.Panel1.Controls.Add(panelIzquierda);
			splitContainer.Panel2.Controls.Add (splitContainerDer);

		}

		private void RealizarBusquedaEnfermedades (string cadena, string filtroBusqueda) {
			switch (filtroBusqueda) {
				case "Nombre":
				BusquedaEnfermedadPorNombre (cadena);
				break;
				case "Tipo":
				BusquedaEnfermedadPorTipo (cadena);
				break;
				default:
				break;
			}
		}

		private void BusquedaEnfermedadPorNombre(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaEnfermedades ();
			} else {
				List<Enfermedad> enf = new List<Enfermedad> ();
				foreach (Enfermedad e in controlador.ListarEnfermedades()) {
					if (e.Nombre.ToLower ().Contains (cadena.ToLower ())) {
						enf.Add (e);
					}
				}
				this.MostrarListaEnfermedadesEnGrid (enf);
			}
		}

		private void BusquedaEnfermedadPorTipo(string cadena) {
			if (cadena.Equals ("")) {
				this.ActualizaEnfermedades ();
			} else {
				List<Enfermedad> enf = new List<Enfermedad> ();
				foreach (Enfermedad e in controlador.ListarEnfermedades()) {
					if (e.Tipo.ToLower ().Contains (cadena.ToLower ())) {
						enf.Add (e);
					}
				}
				this.MostrarListaEnfermedadesEnGrid (enf);
			}
		}

		private void MostrarListaEnfermedadesEnGrid(List<Enfermedad> list) {
			this.grdListaEnfermedades.Rows.Clear ();
			for (int i = 0; i < list.Count; i++) {
				if ( this.grdListaEnfermedades.Rows.Count <= i ) {
					this.grdListaEnfermedades.Rows.Add();
				}

				if ( i < 0
				    || i > this.grdListaEnfermedades.Rows.Count ) {

					throw new ArgumentOutOfRangeException( "fila fuera de rango: " + i );
				}

				DataGridViewRow row = this.grdListaEnfermedades.Rows[ i ];
				Enfermedad enfermedad = list[i];

				row.Cells[ ColId ].Value = enfermedad.Id.ToString().PadLeft( 4, ' ' );
				row.Cells[ ColNombre ].Value = enfermedad.Nombre;
				row.Cells[ ColTipo ].Value = enfermedad.Tipo;
			}
		}

		private void InsertarEnfermedad() {
			this.insModEnfermedad = new InsModEnfermedadWindow (-1, controlador);
			this.insModEnfermedad.Build ();
			this.insModEnfermedad.Show ();
			this.insModEnfermedad.FormClosed += (sender, e) => ActualizaEnfermedades ();
		}

		private void ModificarEnfermedad(int id) {
			this.insModEnfermedad = new InsModEnfermedadWindow (id, controlador);
			this.insModEnfermedad.Build ();
			this.insModEnfermedad.Show ();
			this.insModEnfermedad.FormClosed += (sender, e) => ActualizaEnfermedades ();
		}

		private void BorrarEnfermedad(int id) {
			controlador.BorrarEnfermedad (id);
			ActualizaEnfermedades ();
		}

		private int GetIdEnfermedadSeleccionada() {
			try {
				int actual_row = this.grdListaEnfermedades.CurrentRow.Index;
				return Convert.ToInt32 (this.grdListaEnfermedades.Rows [actual_row].Cells [0].Value);
			}
			catch (NullReferenceException) {
				return -1;
			}
		}

		private void ActualizaFilaDeListaEnfermedades(int rowIndex, Enfermedad e) {
			if ( rowIndex < 0
			    || rowIndex > this.grdListaEnfermedades.Rows.Count ) {

				throw new ArgumentOutOfRangeException( "fila fuera de rango: " + rowIndex );
			}

			DataGridViewRow row = this.grdListaEnfermedades.Rows[ rowIndex ];

			row.Cells[ ColId ].Value = e.Id.ToString().PadLeft( 4, ' ' );
			row.Cells[ ColNombre ].Value = e.Nombre;
			row.Cells[ ColTipo ].Value = e.Tipo;

			return;
		}

		/// <summary>
		/// Método que actualiza el grid en el que se muestran los objetos de tipo Core.Enfermedad.
		/// </summary>
		public void ActualizaEnfermedades() {
			int i = 0;
			List<Enfermedad> lista = controlador.ListarEnfermedades();
			BorrarLista ();
			foreach (Enfermedad enfermedad in lista) {
				this.grdListaEnfermedades.Rows.Add ();
				ActualizaFilaDeListaEnfermedades (i++, enfermedad);
			}

			return;
		}

		private void BorrarLista () {
			this.grdListaEnfermedades.Rows.Clear ();
			this.grdListaEnfermedades.Refresh ();
		}

		private void BuildGridEnfermedades() {
			var grdLista = new DataGridView();
			grdLista.Dock = DockStyle.Fill;
			grdLista.AllowUserToResizeRows = false;
			grdLista.RowHeadersVisible = false;
			grdLista.AutoGenerateColumns = false;
			grdLista.MultiSelect = false;
			grdLista.AllowUserToAddRows = false;
			var textCellTemplate0 = new DataGridViewTextBoxCell();
			var textCellTemplate1 = new DataGridViewTextBoxCell();
			var textCellTemplate2 = new DataGridViewTextBoxCell();
			textCellTemplate0.Style.BackColor = grdLista.RowHeadersDefaultCellStyle.BackColor;
			textCellTemplate1.Style.BackColor = Color.Lavender;
			textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate2.Style.BackColor = Color.Lavender;
			textCellTemplate2.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			var column0 = new DataGridViewTextBoxColumn();
			var column1 = new DataGridViewTextBoxColumn();
			var column2 = new DataGridViewTextBoxColumn();
			column0.SortMode = DataGridViewColumnSortMode.NotSortable;
			column1.SortMode = DataGridViewColumnSortMode.NotSortable;
			column2.SortMode = DataGridViewColumnSortMode.NotSortable;
			column0.CellTemplate = textCellTemplate0;
			column1.CellTemplate = textCellTemplate1;
			column2.CellTemplate = textCellTemplate2;
			column0.HeaderText = "#";
			column0.Width = 50;
			column0.ReadOnly = true;
			column1.HeaderText = "Nombre";
			column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column1.ReadOnly = true;
			column2.HeaderText = "Tipo";
			column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column2.ReadOnly = true;

			grdLista.Columns.AddRange( new DataGridViewColumn[] {
				column0, column1, column2
			} );

			this.grdListaEnfermedades = grdLista;
			ActualizaEnfermedades ();
		}
			
		private Controlador controlador;
		private DataGridView grdListaEnfermedades;
		private InsModEnfermedadWindow insModEnfermedad;
	}
}