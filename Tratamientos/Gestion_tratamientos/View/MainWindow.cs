﻿using System;
using View;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{

	/// <summary>
	/// Clase que representa la ventana principal de la aplicación.
	/// </summary>
	public class MainWindow: Form {

		public const int ColUno = 0;
		public const int ColDos = 1;
		public const int ColTres = 2;
		public const int ColCuatro = 3;
		public const int ColCinco = 4;

		public MainWindow(Controlador controlador) {
			this.controlador = controlador;
			this.BuildGui();
		}

		private void OnQuit() {
			this.Hide();
			this.Close();
		}

		private void BuildMenu() {
			//Menu superior
			this.Menu = new MainMenu();
			var mFile = new MenuItem( "&File" );
			var opQuit = new MenuItem( "&Quit" );
			mFile.MenuItems.Add( opQuit );
			opQuit.Shortcut = Shortcut.CtrlQ;
			opQuit.Click += delegate(object o, EventArgs args) {
				this.OnQuit();
			};

			this.Menu.MenuItems.Add( mFile );
		}

		private void BuildStatus() {
			this.sbStatus = new StatusBar();
			this.sbStatus.Dock = DockStyle.Bottom;
			this.Controls.Add( this.sbStatus );
		}

		private void BuildGui() {
			this.Text = "Gestion de tratamientos";
			this.BuildMenu();
			this.BuildStatus();

			this.tabControl = new TabControl ();
			this.tabControl.Dock = DockStyle.Fill;

			var tratamientosTab = new TratamientosTab (controlador);
			var enfermedadesTab = new EnfermedadesTab (controlador);
			var medicamentosTab = new MedicamentosTab (controlador);
			var pacientesTab = new PacientesTab (controlador);
			this.tabControl.Controls.AddRange(new Control[] { tratamientosTab,enfermedadesTab,medicamentosTab,pacientesTab });
				
			this.Controls.AddRange(new Control[] { this.tabControl });

			this.MinimumSize = new Size( 850, 620 );

			this.tabControl.Click += (sender, e) => {
				tratamientosTab.ActualizaTratamientos();
				medicamentosTab.ActualizaLista();
			};
		}

		private StatusBar sbStatus;
		private TabControl tabControl;
		private Controlador controlador;
	}
}

