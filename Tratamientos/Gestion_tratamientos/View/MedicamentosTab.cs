using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Drawing;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa un objeto <see cref="TabPage"/> con la información de los objetos de tipo <see cref="Core.Medicamento"/>.
	/// </summary>
	public class MedicamentosTab : TabPage
	{

		public const int ColId = 0;
		public const int ColNombre = 1;
		public const int ColEnfermedades = 2;

		private Controlador controlador;
		private InsModMedicamentoWindow insModMedicamento;
		private TableLayoutPanel pnlLista;
		private DataGridView grdListaMedicamentos;

		/// <summary>
		/// Inicializa una nueva instancia de la clase MedicamentosTab.
		/// </summary>
		/// <param name="controlador">Controlador que maneja los eventos.</param>
		public MedicamentosTab (Controlador controlador) {
			this.controlador = controlador;
			this.Build ();
		}

		private void Build() {

			this.Text = "Medicamentos";

			var splitContainer = new SplitContainer();
			splitContainer.SplitterDistance = 110;
			splitContainer.Dock = DockStyle.Fill;
			splitContainer.Orientation = Orientation.Vertical;
			this.Controls.Add (splitContainer);

			//SplitterPanel Izquierda
			var panelIzquierda = new SplitterPanel(splitContainer);
			panelIzquierda.Dock = DockStyle.Fill;
			panelIzquierda.Padding = new Padding(10);

			//Tabla para mostrar datos
			BuildGridMedicamentos ();
			this.grdListaMedicamentos.Dock = DockStyle.Fill;
			panelIzquierda.Controls.Add(this.grdListaMedicamentos);

			//SplitterPanel Derecha
			var splitContainerDer = new SplitContainer();
			splitContainerDer.Dock = DockStyle.Fill;
			splitContainerDer.Padding = new Padding (10);
			splitContainerDer.Orientation = Orientation.Horizontal;

			var splitArriba = new SplitterPanel(splitContainerDer);
			splitArriba.Dock = DockStyle.Fill;

			var tableBuscar = new TableLayoutPanel();
			tableBuscar.Dock = DockStyle.Fill;
			tableBuscar.Padding = new Padding (10);
			tableBuscar.RowCount = 3;

			//Label buscar por:
			var lblBuscarPor = new Label();
			lblBuscarPor.Text = "Buscar por:";
			tableBuscar.Controls.Add(lblBuscarPor);

			//ComboBox elegir tipo busqueda
			var comboBox = new ComboBox();
			comboBox.Dock = DockStyle.Fill;
			comboBox.FlatStyle = FlatStyle.Flat;
			comboBox.Items.Add("Nombre");
			comboBox.Items.Add("Trata");
			comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			comboBox.SelectedIndex =0;
			tableBuscar.Controls.Add(comboBox);

			//Textbox cadena de busqueda
			var tbSearch = new TextBox();
			tbSearch.Width = 100;
			tbSearch.BorderStyle = BorderStyle.FixedSingle;
			tbSearch.Padding = new Padding(10);
			tbSearch.Dock = DockStyle.Fill;
			tbSearch.TextChanged += (sender, e) => this.RealizarBusquedaMedicamentos(tbSearch.Text,comboBox.SelectedItem.ToString());
			tableBuscar.Controls.Add(tbSearch);

			splitArriba.Controls.Add (tableBuscar);

			var splitAbajo = new SplitterPanel(splitContainerDer);
			var tableButInsModBor = new TableLayoutPanel ();
			tableButInsModBor.RowCount = 3;
			tableButInsModBor.Dock = DockStyle.Fill;

			//Boton insertar
			var btInsert = new Button();
			btInsert.Dock = DockStyle.Fill;
			btInsert.Text = "Insertar";
			btInsert.FlatStyle = FlatStyle.Flat;
			btInsert.Click += (sender, e) => {
				this.InsertarMedicamento();
				this.ActualizaMedicamentos();
			};

			//Boton modificar
			var btMod = new Button();
			btMod.Dock = DockStyle.Fill;
			btMod.Text ="Modificar";
			btMod.FlatStyle = FlatStyle.Flat;
			btMod.Click += (sender, e) => {
				int idMedicamentoSeleccionado = Convert.ToInt32 (this.grdListaMedicamentos.CurrentRow.Cells[0].Value);
				if(idMedicamentoSeleccionado==-1) {
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				} else {
					this.ModificarMedicamento (idMedicamentoSeleccionado);
					this.ActualizaMedicamentos();
				}
			};

			//Boton borrar
			var btBor = new Button ();
			btBor.Dock = DockStyle.Top;
			btBor.Text ="Borrar";
			btBor.FlatStyle = FlatStyle.Flat;
			btBor.Click += (sender, e) => {
				int idMedicamentoSeleccionado = Convert.ToInt32 (this.grdListaMedicamentos.CurrentRow.Cells[0].Value);
				if(idMedicamentoSeleccionado==-1) {
					MessageBox.Show("Por favor, seleccione un item","Error",MessageBoxButtons.OK);
				} else {
					this.BorrarMedicamento (idMedicamentoSeleccionado);
					ActualizaMedicamentos ();
				}
			};

			tableButInsModBor.Controls.Add (btInsert);
			tableButInsModBor.Controls.Add (btMod);
			tableButInsModBor.Controls.Add (btBor);

			splitAbajo.Controls.Add (tableButInsModBor);

			splitContainerDer.Panel1.Controls.Add (splitArriba);
			splitContainerDer.Panel2.Controls.Add (splitAbajo);
			splitContainer.Panel1.Controls.Add(panelIzquierda);
			splitContainer.Panel2.Controls.Add (splitContainerDer);

		}

		private void RealizarBusquedaMedicamentos (string valor, string tipo) {
			List<Medicamento> lista;
			if (valor != "") {
				lista = controlador.ListarMedicamentos (tipo, valor);

			} else {
				lista = controlador.ListarMedicamentos();
			}
			ActualizaLista(lista);	// crear uno o modificar el que esta
		}


		private void InsertarMedicamento() {
			this.insModMedicamento = new InsModMedicamentoWindow (-1, controlador);
			this.insModMedicamento.Build ();
			this.insModMedicamento.Show ();
			this.insModMedicamento.FormClosed += (sender, e) => ActualizaMedicamentos ();
		}

		private void ModificarMedicamento(int id) {
			this.insModMedicamento = new InsModMedicamentoWindow (id, controlador);
			this.insModMedicamento.Build ();
			this.insModMedicamento.Show ();
			this.insModMedicamento.FormClosed += (sender, e) => ActualizaMedicamentos ();
		}

		private void BorrarMedicamento(int id) {
			controlador.BorrarMedicamento (id);
			ActualizaMedicamentos ();
		}

		private int GetIdMedicamentoSeleccionado() {
			try {
				int actual_row = this.grdListaMedicamentos.CurrentRow.Index;
				return Convert.ToInt32 (this.grdListaMedicamentos.Rows [actual_row].Cells [0].Value);
			}
			catch (NullReferenceException) {
				return -1;
			}
		}

		private void ActualizaFilaDeListaMedicamentos(int rowIndex, Medicamento m) {
			if ( rowIndex < 0
			    || rowIndex > this.grdListaMedicamentos.Rows.Count ) {

				throw new ArgumentOutOfRangeException( "fila fuera de rango: " + rowIndex );
			}

			DataGridViewRow row = this.grdListaMedicamentos.Rows[ rowIndex ];

			row.Cells[ ColId ].Value = m.Id.ToString().PadLeft( 4, ' ' );
			row.Cells[ ColNombre ].Value = m.Nombre;
			row.Cells[ ColEnfermedades ].Value = m.ListadoEnfermedades();

			return;
		}

		/// <summary>
		/// Método que actualiza el grid en el que se muestran los objetos de tipo Core.Medicamento.
		/// </summary>
		public void ActualizaMedicamentos() {
			int i = 0;
			List<Medicamento> lista = controlador.ListarMedicamentos();
			BorrarLista ();
			foreach (Medicamento medicamento in lista) {
				this.grdListaMedicamentos.Rows.Add ();
				ActualizaFilaDeListaMedicamentos (i++, medicamento);
			}

			return;
		}

		private void BorrarLista () {
			this.grdListaMedicamentos.Rows.Clear ();
			this.grdListaMedicamentos.Refresh ();
		}

		private void BuildGridMedicamentos() {
			this.pnlLista = new TableLayoutPanel ();
			this.pnlLista.Padding = new Padding (5, 15, 5, 5);
			this.pnlLista.RowCount = 2;
			this.pnlLista.AutoSize = true;
			this.pnlLista.SuspendLayout ();
			this.pnlLista.Dock = DockStyle.Fill;

			// Crear gridview
			this.grdListaMedicamentos = new DataGridView ();
			this.grdListaMedicamentos.Dock = DockStyle.Fill;
			this.grdListaMedicamentos.AllowUserToResizeRows = false;
			this.grdListaMedicamentos.RowHeadersVisible = false;
			this.grdListaMedicamentos.AutoGenerateColumns = false;
			this.grdListaMedicamentos.MultiSelect = false;
			this.grdListaMedicamentos.AllowUserToAddRows = false;
			var textCellTemplate0 = new DataGridViewTextBoxCell ();
			var textCellTemplate1 = new DataGridViewTextBoxCell ();
			var textCellTemplate2 = new DataGridViewTextBoxCell ();
			textCellTemplate0.Style.BackColor = this.grdListaMedicamentos.RowHeadersDefaultCellStyle.BackColor;
			textCellTemplate1.Style.BackColor = Color.Lavender;
			textCellTemplate1.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			textCellTemplate2.Style.BackColor = Color.Lavender;
			textCellTemplate2.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
			var column0 = new DataGridViewTextBoxColumn ();
			var column1 = new DataGridViewTextBoxColumn ();
			var column2 = new DataGridViewTextBoxColumn ();
			column0.SortMode = DataGridViewColumnSortMode.NotSortable;
			column1.SortMode = DataGridViewColumnSortMode.NotSortable;
			column2.SortMode = DataGridViewColumnSortMode.NotSortable;
			column0.CellTemplate = textCellTemplate0;
			column1.CellTemplate = textCellTemplate1;
			column2.CellTemplate = textCellTemplate2;
			column0.HeaderText = "#";
			column0.Width = 50;	
			column0.ReadOnly = true;
			column1.HeaderText = "Nombre";
			column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column1.ReadOnly = true;
			column2.HeaderText = "Trata: ";
			column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			column2.ReadOnly = true;

			this.grdListaMedicamentos.Columns.AddRange (new DataGridViewColumn[] {
				column0, column1, column2 
			});

			pnlLista.ResumeLayout (false);
		}

		private void Buscar (string tipo, string valor) {
			List<Medicamento> lista;
			if (valor != "") {
				lista = controlador.ListarMedicamentos (tipo, valor);

			} else {
				lista = controlador.ListarMedicamentos();
			}
			ActualizaLista(lista);	// crear uno o modificar el que esta
		}

		public void ActualizaLista () {
			int i = 0;
			List<Medicamento> lista = controlador.ListarMedicamentos ();
			BorrarLista ();
			foreach (Medicamento medicamento in lista) {
				this.grdListaMedicamentos.Rows.Add();
				ActualizaFilaDeLista(i++, medicamento);
			}
		}

		private void ActualizaFilaDeLista (int i, Medicamento medicamento) {
			DataGridViewRow row = this.grdListaMedicamentos.Rows [i];
			row.Cells[ColId].Value = medicamento.Id.ToString ();
			row.Cells[ColNombre].Value = medicamento.Nombre;
			row.Cells[ColEnfermedades].Value = medicamento.ListadoEnfermedades();
		}

		private void ActualizaLista (List<Medicamento> lista) {
			int i = 0;
			BorrarLista ();
			foreach (Medicamento medicamento in lista) {
				this.grdListaMedicamentos.Rows.Add();
				ActualizaFilaDeLista(i++, medicamento);
			}
		}


	}
}