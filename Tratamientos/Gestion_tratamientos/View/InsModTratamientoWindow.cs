using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using Gestion_tratamientos.Utils;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa la ventana en la que se insertan o modifican objetos de tipo <see cref="Core.Tratamiento"/>.
	/// </summary>
	public class InsModTratamientoWindow : Form
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase InsModTratamientoWindow.
		/// </summary>
		/// <param name="id">Id del objeto Tratamiento a modificar (-1 cuando se inserta).</param>
		/// <param name="controlador">Controlador que inserta/modifica el objeto en el modelo.</param>
		public InsModTratamientoWindow (int id, Controlador controlador) {
			this.id = id;
			this.controlador = controlador;
		}

		/// <summary>
		/// Método que construye la ventana en la que el usuario introduce los datos.
		/// </summary>
		public void Build() {

			if (id == -1) {
				this.Text = "Insertar nuevo tratamiento";
				this.modificando = false;
			} else {
				this.modificando = true;
				this.Text = "Modificar tratamiento #"+id.ToString();
				this.tratamientoAModificar = controlador.ObtenerTratamiento (id);
			}
			this.insModPanel = new TableLayoutPanel();
			this.insModPanel.Padding = new Padding (10);
			this.insModPanel.RowCount = 5;

			this.insModPanel.Dock = DockStyle.Fill;

			//Duracion
			this.duracionPanel = new Panel ();
			this.duracionPanel.Height = 20;
			this.duracionPanel.Dock = DockStyle.Fill;
			this.duracionLabel = new Label ();
			this.duracionLabel.Text = "Duración (días):";
			this.duracionLabel.Dock = DockStyle.Left;
			this.duracionPanel.Controls.Add (duracionLabel);
			this.duracionTextBox = new TextBox ();
			if (modificando) {
				this.duracionTextBox.Text = tratamientoAModificar.Duracion.ToString();
			}
			this.duracionTextBox.Dock = DockStyle.Right;
			this.duracionTextBox.Width = 200;
			this.duracionPanel.Controls.Add (duracionTextBox);

			//Medicamentos
			this.medicamentosPanel = new Panel ();
			this.medicamentosPanel.Dock = DockStyle.Fill;
			this.medicamentosLabel = new Label ();
			this.medicamentosLabel.Text = "Medicamentos:";
			this.medicamentosLabel.Dock = DockStyle.Left;
			this.medicamentosPanel.Controls.Add (medicamentosLabel);

			this.medicamentosCheckBox = new CheckedListBox();

			this.medicamentosCheckBox.SelectedIndexChanged += (sender, e) => this.actualizarComboBox();

			bool ischecked = false;

			foreach(Medicamento m in controlador.ListarMedicamentos()) {

				if(modificando) {
					if(tratamientoAModificar.ListaMedicamentos.Contains(m)) {
						ischecked = true;
					} else {
						ischecked = false;
					}
				}

				medicamentosCheckBox.Items.Add(new Thing() {
					Key = m.Id.ToString(),
					Value = m.Nombre
				}, ischecked);
			}

			this.medicamentosCheckBox.Dock = DockStyle.Right;
			this.medicamentosCheckBox.Width = 200;
			this.medicamentosPanel.Controls.Add (medicamentosCheckBox);

			//Enfermedad
			this.enfermedadPanel = new Panel ();
			this.enfermedadPanel.Height = 20;
			this.enfermedadPanel.Dock = DockStyle.Fill;
			this.enfermedadLabel = new Label ();
			this.enfermedadLabel.Text = "Enfermedad:";
			this.enfermedadLabel.Dock = DockStyle.Left;
			this.enfermedadPanel.Controls.Add (enfermedadLabel);

			this.enfermedadComboBox = new ComboBox ();

			foreach (Enfermedad e in controlador.ListarEnfermedades()) {
				enfermedadComboBox.Items.Add(new Thing() {
					Key = e.Id.ToString(),
					Value = e.Nombre
				});
			}

			if (modificando) {
				enfermedadComboBox.Text = tratamientoAModificar.Enfermedad.Nombre;
			}

			this.enfermedadComboBox.Dock = DockStyle.Right;
			this.enfermedadComboBox.Width = 200;
			this.enfermedadPanel.Controls.Add (enfermedadComboBox);

			//Paciente
			this.pacientePanel = new Panel();
			this.pacientePanel.Height = 20;
			this.pacientePanel.Dock = DockStyle.Fill;
			this.pacienteLabel = new Label ();
			this.pacienteLabel.Text = "Paciente";
			this.pacienteLabel.Dock = DockStyle.Left;
			this.pacientePanel.Controls.Add(pacienteLabel);

			this.pacienteComboBox = new ComboBox ();

			foreach (Paciente p in controlador.ListarPacientes()) {
				pacienteComboBox.Items.Add(new Thing() {
					Key = p.Id.ToString(),
					Value = p.Nombre
				});
			}

			if (modificando) {
				pacienteComboBox.Text = tratamientoAModificar.Paciente.Nombre;
			}

			this.pacienteComboBox.Dock = DockStyle.Right;
			this.pacienteComboBox.Width = 200;
			this.pacientePanel.Controls.Add (pacienteComboBox);

			//Botones
			this.botonesPanel = new TableLayoutPanel ();
			this.botonesPanel.Height = 30;
			this.botonesPanel.ColumnCount = 2;
			this.botonAceptar = new Button ();
			this.botonAceptar.Size = new Size (80,25);
			this.botonAceptar.Text = "Aceptar";
			this.botonAceptar.Dock = DockStyle.Left;

			this.botonAceptar.Click += (sender, e) => {
				try{
					Convert.ToInt32(this.duracionTextBox.Text);
				}
				catch (FormatException){
					MessageBox.Show("Formato de duracion incorrecto");
					return;
				}
				if(modificando){
					this.ModificarTratamiento ();
				}
				else{
					this.InsertarTratamiento ();
				}
				this.Hide ();
				this.Close ();
			};

			this.botonesPanel.SetColumn(botonAceptar,1);
			this.botonesPanel.Controls.Add (botonAceptar);
			this.botonCancelar = new Button ();
			this.botonCancelar.Size = new Size (80,25);
			this.botonCancelar.Text = "Cancelar";
			this.botonCancelar.Click += (sender, e) => {
				this.Hide ();
				this.Close ();
			};
			this.botonCancelar.Dock = DockStyle.Right;
			this.botonesPanel.SetColumn(botonCancelar,2);
			this.botonesPanel.Controls.Add (botonCancelar);

			this.insModPanel.Controls.Add (duracionPanel);
			this.insModPanel.Controls.Add (medicamentosPanel);
			this.insModPanel.Controls.Add (enfermedadPanel);
			this.insModPanel.Controls.Add (pacientePanel);
			this.insModPanel.Controls.Add (botonesPanel);

			this.MinimumSize = new Size (400, 300);
			this.Controls.Add (insModPanel);

		}

		private void InsertarTratamiento() {
			int checkedId = 0;

			int idPaciente = Convert.ToInt32(((Thing) pacienteComboBox.SelectedItem).Key);
			int idEnfermedad = Convert.ToInt32(((Thing) enfermedadComboBox.SelectedItem).Key);
			List<Medicamento> medicamentos = new List<Medicamento>();
		
			int duracion = Convert.ToInt32(duracionTextBox.Text);

			Paciente paciente = controlador.ObtenerPaciente (idPaciente);
			Enfermedad enfermedad = controlador.ObtenerEnfermedad(idEnfermedad);

			foreach (var s in medicamentosCheckBox.CheckedItems) {
				checkedId = Convert.ToInt32(((Thing)s).Key);
				medicamentos.Add (controlador.ObtenerMedicamento(checkedId));
			}

			controlador.InsertarTratamiento(duracion, medicamentos, enfermedad, paciente);
		}

		private void ModificarTratamiento() {
			int checkedId = 0;

			int idPaciente = Convert.ToInt32(((Thing) pacienteComboBox.SelectedItem).Key);
			int idEnfermedad = Convert.ToInt32(((Thing) enfermedadComboBox.SelectedItem).Key);
			List<Medicamento> medicamentos = new List<Medicamento>();
			int duracion = Convert.ToInt32(duracionTextBox.Text);
			int idViejo = tratamientoAModificar.Id;

			Paciente paciente = controlador.ObtenerPaciente (idPaciente);
			Enfermedad enfermedad = controlador.ObtenerEnfermedad(idEnfermedad);

			foreach (var s in medicamentosCheckBox.CheckedItems) {
				checkedId = Convert.ToInt32(((Thing)s).Key);
				medicamentos.Add (controlador.ObtenerMedicamento(checkedId));
			}

			controlador.ModificarTratamiento(idViejo, duracion, medicamentos, enfermedad, paciente);
		}


		private void actualizarComboBox(){
			int checkedId;
			List<Enfermedad> listaEnfermedades = new List<Enfermedad> ();
			foreach (var s in medicamentosCheckBox.CheckedItems) {
				checkedId = Convert.ToInt32(((Thing)s).Key);
				foreach (var e in controlador.ObtenerMedicamento(checkedId).ListaEnfermedades) {
					if (!listaEnfermedades.Contains(e)){
						listaEnfermedades.Add (e);
					}
				}
			}
			this.enfermedadComboBox.Items.Clear();
			foreach (Enfermedad e in listaEnfermedades) {

				this.enfermedadComboBox.Items.Add(new Thing() {
					Key = e.Id.ToString(),
					Value = e.Nombre
				});
			}
		}

		private TableLayoutPanel insModPanel;
		private Panel duracionPanel;
		private Label duracionLabel;
		private TextBox duracionTextBox;
		private Panel medicamentosPanel;
		private Label medicamentosLabel;
		private CheckedListBox medicamentosCheckBox;
		private Panel enfermedadPanel;
		private Label enfermedadLabel;
		private ComboBox enfermedadComboBox;
		private Panel pacientePanel;
		private Label pacienteLabel;
		private ComboBox pacienteComboBox;
		private TableLayoutPanel botonesPanel;
		private Button botonAceptar;
		private Button botonCancelar;

		private int id;
		private Controlador controlador;

		private bool modificando;
		private Tratamiento tratamientoAModificar;

	}
}