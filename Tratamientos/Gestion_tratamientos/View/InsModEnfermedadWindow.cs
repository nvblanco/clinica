using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.View
{
	/// <summary>
	/// Clase que representa la ventana en la que se insertan o modifican objetos de tipo <see cref="Core.Enfermedad"/>.
	/// </summary>
	public class InsModEnfermedadWindow : Form
	{
		/// <summary>
		/// Inicializa una nueva instancia de la clase InsModEnfermedadWindow.
		/// </summary>
		/// <param name="id">Id del objeto Enfermedad a modificar (-1 cuando se inserta).</param>
		/// <param name="controlador">Controlador que inserta/modifica el objeto en el modelo.</param>
		public InsModEnfermedadWindow (int id, Controlador controlador) {
			this.id = id;
			this.controlador = controlador;
		}

		/// <summary>
		/// Método que construye la ventana en la que el usuario introduce los datos.
		/// </summary>
		public void Build() {

			if (id == -1) {
				this.Text = "Insertar nueva enfermedad";
				this.modificando = false;
			} else {
				this.modificando = true;
				this.Text = "Modificar enfermedad #"+id.ToString();
				this.enfermedadAModificar = controlador.ObtenerEnfermedad (id);
			}
			this.insModPanel = new TableLayoutPanel();
			this.insModPanel.Padding = new Padding (10);
			this.insModPanel.RowCount = 5;

			this.insModPanel.Dock = DockStyle.Fill;

			//Nombre
			this.nombrePanel = new Panel ();
			this.nombrePanel.Height = 20;
			this.nombrePanel.Dock = DockStyle.Fill;
			this.nombreLabel = new Label ();
			this.nombreLabel.Text = "Nombre:";
			this.nombreLabel.Dock = DockStyle.Left;
			this.nombrePanel.Controls.Add (nombreLabel);
			this.nombreTextBox = new TextBox ();
			if (modificando) {
				this.nombreTextBox.Text = enfermedadAModificar.Nombre.ToString();
			}
			this.nombreTextBox.Dock = DockStyle.Right;
			this.nombreTextBox.Width = 200;
			this.nombrePanel.Controls.Add (nombreTextBox);


			//Paciente
			this.tipoPanel = new Panel();
			this.tipoPanel.Height = 20;
			this.tipoPanel.Dock = DockStyle.Fill;
			this.tipoLabel = new Label ();
			this.tipoLabel.Text = "Tipo";
			this.tipoLabel.Dock = DockStyle.Left;
			this.tipoPanel.Controls.Add (tipoLabel);
			this.tipoComboBox = new ComboBox ();
			this.tipoComboBox.Items.Add ("Virus");
			this.tipoComboBox.Items.Add ("Bacteria");

			if (modificando) {
				this.tipoComboBox.Text = controlador.ObtenerEnfermedad (enfermedadAModificar.Id).Tipo;
			}
			this.tipoComboBox.Dock = DockStyle.Right;
			this.tipoComboBox.Width = 200;
			this.tipoPanel.Controls.Add (tipoComboBox);

			//Botones
			this.botonesPanel = new TableLayoutPanel ();
			this.botonesPanel.Height = 30;
			this.botonesPanel.ColumnCount = 2;
			this.botonAceptar = new Button ();
			this.botonAceptar.Size = new Size (80,25);
			this.botonAceptar.Text = "Aceptar";
			this.botonAceptar.Dock = DockStyle.Left;
			if (modificando) {
				this.botonAceptar.Click += (sender, e) => {
					this.ModificarEnfermedad ();
					this.Hide ();
					this.Close ();
				};
			} else {
				this.botonAceptar.Click += (sender, e) => {
					this.InsertarEnfermedad ();
					this.Hide ();
					this.Close ();
				};
			}
			this.botonesPanel.SetColumn(botonAceptar,1);
			this.botonesPanel.Controls.Add (botonAceptar);
			this.botonCancelar = new Button ();
			this.botonCancelar.Size = new Size (80,25);
			this.botonCancelar.Text = "Cancelar";
			this.botonCancelar.Click += (sender, e) => {
				this.Hide ();
				this.Close ();
			};
			this.botonCancelar.Dock = DockStyle.Right;
			this.botonesPanel.SetColumn(botonCancelar,2);
			this.botonesPanel.Controls.Add (botonCancelar);

			this.insModPanel.Controls.Add (nombrePanel);
			this.insModPanel.Controls.Add (tipoPanel);
			this.insModPanel.Controls.Add (botonesPanel);

			this.MinimumSize = new Size (400, 300);
			this.Controls.Add (insModPanel);

		}

		private void InsertarEnfermedad() {
			int id = controlador.NumEnfermedades + 1 ;
			string nombre = nombreTextBox.Text;
			string tipo = tipoComboBox.SelectedItem.ToString();
			Enfermedad e = new Enfermedad (id, nombre, tipo);
			controlador.InsertarEnfermedad (e);
		}

		private void ModificarEnfermedad() {
			int idViejo = enfermedadAModificar.Id;
			string nombre = nombreTextBox.Text;
			string tipo = tipoComboBox.SelectedItem.ToString();
			Enfermedad e = new Enfermedad (id, nombre, tipo);
			controlador.ModificarEnfermedad (idViejo,e);
		}

		private TableLayoutPanel insModPanel;
		private Panel nombrePanel;
		private Label nombreLabel;
		private TextBox nombreTextBox;
		private Panel tipoPanel;
		private Label tipoLabel;
		private ComboBox tipoComboBox;
		private TableLayoutPanel botonesPanel;
		private Button botonAceptar;
		private Button botonCancelar;

		private int id;
		private Controlador controlador;

		private bool modificando;
		private Enfermedad enfermedadAModificar;

	}
}

