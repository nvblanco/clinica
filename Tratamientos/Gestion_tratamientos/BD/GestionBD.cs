﻿using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase fachada que realiza las llamadas necesarias para cargar los datos en el arranque de la aplicación.
	/// </summary>
	class GestionBD
	{
		/// <summary>
		/// Método que usa un objeto de tipo Core.GestionBDPacientes para realizar la carga de los datos de la BD.
		/// </summary>
		/// <returns>Diccionario con los objetos de tipo Core.Paciente.</returns>
		public SortedDictionary<int, Paciente> SetUpPacientes()
		{
			GestionBDPacientes gestionpacientes = new GestionBDPacientes();
			dictionarypacientes = gestionpacientes.Load();
			return dictionarypacientes;
		}

		/// <summary>
		/// Método que usa un objeto de tipo Core.GestionBDEnfermedades para realizar la carga de los datos de la BD.
		/// </summary>
		/// <returns>Diccionario con los objetos de tipo Core.Enfermedad.</returns>
		public SortedDictionary<int, Enfermedad> SetUpEnfermedades()
		{
			GestionBDEnfermedades gestionenfermedades = new GestionBDEnfermedades();
			dictionaryenfermedades = gestionenfermedades.Load();
			return dictionaryenfermedades;
		}

		/// <summary>
		/// Método que usa un objeto de tipo Core.GestionBDMedicamentos para realizar la carga de los datos de la BD.
		/// </summary>
		/// <returns>Diccionario con los objetos de tipo Core.Medicamento.</returns>
		public SortedDictionary<int, Medicamento> SetUpMedicamentos()
		{
			GestionBDMedicamentos gestionmedicamentos = new GestionBDMedicamentos();
			dictionarymedicamentos = gestionmedicamentos.Load(dictionaryenfermedades);
			return dictionarymedicamentos;
		}

		/// <summary>
		/// Método que usa un objeto de tipo Core.GestionBDTratamientos para realizar la carga de los datos de la BD.
		/// </summary>
		/// <returns>Diccionario con los objetos de tipo Core.Tratamiento.</returns>
		public SortedDictionary<int, Tratamiento> SetUpTratamientos()
		{
			GestionBDTratamientos gestiontratamientos = new GestionBDTratamientos();
			dictionarytratamientos = gestiontratamientos.Load(dictionarypacientes, dictionaryenfermedades, dictionarymedicamentos);
			return dictionarytratamientos;
		}

		SortedDictionary<int, Paciente> dictionarypacientes;
		SortedDictionary<int, Enfermedad> dictionaryenfermedades;
		SortedDictionary<int, Medicamento> dictionarymedicamentos;
		SortedDictionary<int, Tratamiento> dictionarytratamientos;
	}
}