﻿using System.Data;
//using System.Data.SQLite;
using Mono.Data.Sqlite;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase encargada de realizar la conexión y desconexión con la BD.
	/// </summary>
    class BDConnection
    {
        protected void Open() {
            dbcon = new SqliteConnection("Data Source=../../BD/Clinica.db;Version=3;New=False;Compress=True;");
            dbcon.Open();
        }

        protected void Close() {
            dbcmd.Dispose();
            dbcmd = null;
            dbcon.Close();
            dbcon = null;
        }

		protected SqliteConnection dbcon;
		protected SqliteCommand dbcmd;
        protected IDataReader reader;
    }
}
