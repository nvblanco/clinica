﻿using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, modificaciones, borrados y consultas de objetos de tipo <see cref="Core.Paciente"/> en la BD.
	/// <seealso cref="GestionBD"/>
	/// </summary>
    class GestionBDPacientes : BDConnection
    {
		/// <summary>
		/// Método encargado de cargar el diccionario de objetos de tipo Core.Paciente desde la BD.
		/// </summary>
        public SortedDictionary<int, Paciente> Load() {
            SortedDictionary<int, Paciente> dictionary = new SortedDictionary<int, Paciente>();

            Open();

            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM PACIENTES";

            reader = dbcmd.ExecuteReader();

            while (reader.Read()) {
                int idpaciente = reader.GetInt32(0);
                string nombrepaciente = reader.GetString(1);
                string apellidospaciente = reader.GetString(2);
                string dnipaciente = reader.GetString(3);
                string fechanacimientoPaciente = reader.GetString(4);

                Paciente p = new Paciente(idpaciente, nombrepaciente, apellidospaciente, dnipaciente, fechanacimientoPaciente);

                dictionary.Add(idpaciente, p);
            }

			reader.Close();
			reader = null;

            Close();

            return dictionary;
        }


		/// <summary>
		/// Metódo que recibe un objeto de tipo Core.Paciente e inserta sus datos en la BD.
		/// </summary>
		/// <param name="p">Objeto de tipo Core.Paciente.</param>
		public void Insert(Paciente p) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "INSERT INTO PACIENTES (idPaciente, nombrePaciente, apellidosPaciente, dniPaciente, fechaNacimientoPaciente) " +
				"VALUES (@Id, @Nombre, @Apellidos, @DNI, @FechaNac)";
			dbcmd.Parameters.AddWithValue("@Id", p.Id);
			dbcmd.Parameters.AddWithValue("@Nombre", p.Nombre);
			dbcmd.Parameters.AddWithValue("@Apellidos", p.Apellidos);
			dbcmd.Parameters.AddWithValue("@DNI", p.DNI);
			dbcmd.Parameters.AddWithValue("@FechaNac", p.FechaNac);

			dbcmd.ExecuteNonQuery();

			Close();
		}

		/// <summary>
		/// Método que recibe el id de una enfermedad y un objeto de tipo Core.Paciente y actualiza los datos de dicho objeto en la BD.
		/// </summary>
		/// <param name="id">Id del paciente.</param>
		/// <param name="p">Objeto de tipo Core.Paciente.</param>
		public void Update(int id, Paciente p) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "UPDATE PACIENTES SET nombrePaciente = @Nombre, apellidosPaciente = @Apellidos, dniPaciente = @DNI, fechaNacimientoPaciente = @FechaNac " +
				"WHERE idPaciente = @id";
			dbcmd.Parameters.AddWithValue("@Nombre", p.Nombre);
			dbcmd.Parameters.AddWithValue("@Apellidos", p.Apellidos);
			dbcmd.Parameters.AddWithValue("@DNI", p.DNI);
			dbcmd.Parameters.AddWithValue("@FechaNac", p.FechaNac);
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}

		/// <summary>
		/// Método que borra de la BD los datos del objeto de tipo Core.Paciente cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del paciente a borrar.</param>
		public void Delete(int id) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM PACIENTES WHERE idPaciente = @Id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}
    }
}
