﻿using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, modificaciones, borrados y consultas de objetos de tipo <see cref="Core.Medicamento"/> en la BD.
	/// <seealso cref="GestionBD"/>
	/// </summary>
    class GestionBDMedicamentos : BDConnection
    {
		/// <summary>
		/// Método encargado de cargar el diccionario de objetos de tipo Core.Medicamento desde la BD.
		/// Para ello se usa un diccionario de objetos de tipo Core.Enfermedad para que se realice la asociación Core.Medicamento trata Core.Enfermedad
		/// <param name="dictionaryEnfermedades">Diccionario con los objetos de tipo Core.Enfermedad</param>
		/// </summary>
        public SortedDictionary<int, Medicamento> Load(SortedDictionary<int, Enfermedad> dictionaryEnfermedades) {
            SortedDictionary<int, Medicamento> dictionaryMedicamentos = new SortedDictionary<int, Medicamento>();

            Open();

            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM MEDICAMENTOS";

            reader = dbcmd.ExecuteReader();

            while (reader.Read()) {
                int idmedicamento = reader.GetInt32(0);
                string nombremedicamento = reader.GetString(1);

                Medicamento m = new Medicamento(idmedicamento, nombremedicamento, new List<Enfermedad>());

                dictionaryMedicamentos.Add(idmedicamento, m);
            }

            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM TRATA";

            reader = dbcmd.ExecuteReader();

            while (reader.Read()) {
                int idenfermedad = reader.GetInt32(0);
                int idmedicamento = reader.GetInt32(1);

                Enfermedad e = dictionaryEnfermedades[idenfermedad];

                dictionaryMedicamentos[idmedicamento].ListaEnfermedades.Add(e);
            }

			reader.Close();
			reader = null;

            Close();

            return dictionaryMedicamentos;
        }

		/// <summary>
		/// Metódo que recibe un objeto de tipo Core.Medicamento e inserta sus datos en la BD.
		/// </summary>
		/// <param name="m">Objeto de tipo Core.Medicamento.</param>
		public void Insert(Medicamento m) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "INSERT INTO MEDICAMENTOS (idMedicamento, nombreMedicamento) VALUES (@Id, @Nombre)";
			dbcmd.Parameters.AddWithValue("@Id", m.Id);
			dbcmd.Parameters.AddWithValue("@Nombre", m.Nombre);

			dbcmd.ExecuteNonQuery();

			foreach(Enfermedad e in m.ListaEnfermedades) {
				dbcmd.CommandText = "INSERT INTO TRATA (idEnfermedad, idMedicamento) VALUES (@IdE, @idM)";
				dbcmd.Parameters.AddWithValue("@IdE", e.Id);
				dbcmd.Parameters.AddWithValue("@IdM", m.Id);

				dbcmd.ExecuteNonQuery();
			}

			Close();
		}

		/// <summary>
		/// Método que recibe un objeto de tipo Core.Medicamento y actualiza los datos de dicho objeto en la BD.
		/// </summary>
		/// <param name="m">Objeto de tipo Core.Medicamento.</param>
		public void Update(Medicamento m) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "UPDATE MEDICAMENTOS SET nombreMedicamento = @Nombre WHERE idMedicamento = @id";
			dbcmd.Parameters.AddWithValue("@Id", m.Id);
			dbcmd.Parameters.AddWithValue("@Nombre", m.Nombre);

			dbcmd.ExecuteNonQuery();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM TRATA WHERE idMedicamento = @id";
			dbcmd.Parameters.AddWithValue("@Id", m.Id);

			dbcmd.ExecuteNonQuery();

			foreach(Enfermedad e in m.ListaEnfermedades) {
				dbcmd.CommandText = "INSERT INTO TRATA (idEnfermedad, idMedicamento) VALUES (@IdE, @idM)";
				dbcmd.Parameters.AddWithValue("@IdE", e.Id);
				dbcmd.Parameters.AddWithValue("@IdM", m.Id);

				dbcmd.ExecuteNonQuery();
			}

			Close();
		}

		/// <summary>
		/// Método que borra de la BD los datos del objeto de tipo Core.Medicamento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del medicamento a borrar.</param>
		public void Delete(int id) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM MEDICAMENTOS WHERE idMedicamento = @Id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM TRATA WHERE idMedicamento = @id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}
    }
}
