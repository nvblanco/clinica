using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, modificaciones, borrados y consultas de objetos de tipo <see cref="Core.Tratamiento"/> en la BD.
	/// <seealso cref="GestionBD"/>
	/// </summary>
	class GestionBDTratamientos : BDConnection
	{
		/// <summary>
		/// Método encargado de cargar el diccionario de objetos de tipo Core.Tratamiento desde la BD.
		/// Para ello se usa un diccionario de objetos de tipo Core.Paciente, otro con objetos de tipo Core.Enfermedad y otro con objetos de tipo Core.Medicamento para que se realice las asociaciones oportunas.
		/// <param name="dictionaryPaciente">Diccionario con los objetos de tipo Core.Paciente</param>
		/// <param name="dictionaryEnfermedad">Diccionario con los objetos de tipo Core.Enfermedad</param>
		/// <param name="dictionaryMedicamento">Diccionario con los objetos de tipo Core.Medicamento</param>
		/// </summary>
		public SortedDictionary<int, Tratamiento> Load(SortedDictionary<int, Paciente> dictionaryPaciente, SortedDictionary<int, Enfermedad> dictionaryEnfermedad, 
			SortedDictionary<int, Medicamento> dictionaryMedicamento) {

            SortedDictionary<int, Tratamiento> dictionaryTratamiento = new SortedDictionary<int, Tratamiento>();

            Open();

            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM TRATAMIENTOS";

            reader = dbcmd.ExecuteReader();

            while (reader.Read()) {
                int idtratamiento = reader.GetInt32(0);
                int idenfermedad = reader.GetInt32(1);
				int idpaciente = reader.GetInt32(2);
				int duraciontratamiento = reader.GetInt32(3);

                Tratamiento e = new Tratamiento(idtratamiento, duraciontratamiento, new List<Medicamento>(), dictionaryEnfermedad[idenfermedad], dictionaryPaciente[idpaciente]);

				dictionaryTratamiento.Add(idtratamiento, e);
            }

			dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM RECETAS";

            reader = dbcmd.ExecuteReader();

            while (reader.Read()) {
                int idtratamiento = reader.GetInt32(0);
                int idmedicamento = reader.GetInt32(1);

				dictionaryTratamiento[idtratamiento].ListaMedicamentos.Add(dictionaryMedicamento[idmedicamento]);
            }

			reader.Close();
			reader = null;

            Close();

            return dictionaryTratamiento;
        }

		/// <summary>
		/// Metódo que recibe un objeto de tipo Core.Tratamiento e inserta sus datos en la BD.
		/// </summary>
		/// <param name="t">Objeto de tipo Core.Tratamiento.</param>
		public void Insert(Tratamiento t) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "INSERT INTO TRATAMIENTOS (idTratamiento, idEnfermedad, idPaciente, duracionTratamiento) VALUES (@IdT, @IdE, @IdP, @duracion)";
			dbcmd.Parameters.AddWithValue("@IdT", t.Id);
			dbcmd.Parameters.AddWithValue("@IdE", t.Enfermedad.Id);
			dbcmd.Parameters.AddWithValue("@IdP", t.Paciente.Id);
			dbcmd.Parameters.AddWithValue("@duracion", t.Duracion);

			dbcmd.ExecuteNonQuery();

			foreach(Medicamento m in t.ListaMedicamentos) {
				dbcmd.CommandText = "INSERT INTO RECETAS (idTratamiento, idMedicamento) VALUES (@IdT, @idM)";
				dbcmd.Parameters.AddWithValue("@IdT", t.Id);
				dbcmd.Parameters.AddWithValue("@IdM", m.Id);

				dbcmd.ExecuteNonQuery();
			}

			Close();
		}

		/// <summary>
		/// Método que recibe un objeto de tipo Core.Tratamiento y actualiza los datos de dicho objeto en la BD.
		/// </summary>
		/// <param name="t">Objeto de tipo Core.Tratamiento.</param>
		public void Update(Tratamiento t) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "UPDATE TRATAMIENTOS SET idEnfermedad = @IdE, idPaciente = @IdP, duracionTratamiento = @duracion WHERE idTratamiento = @idT";
			dbcmd.Parameters.AddWithValue("@IdE", t.Enfermedad.Id);
			dbcmd.Parameters.AddWithValue("@IdP", t.Paciente.Id);
			dbcmd.Parameters.AddWithValue("@duracion", t.Duracion);
			dbcmd.Parameters.AddWithValue("@IdT", t.Id);

			dbcmd.ExecuteNonQuery();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM RECETAS WHERE idTratamiento = @id";
			dbcmd.Parameters.AddWithValue("@Id", t.Id);

			dbcmd.ExecuteNonQuery();

			foreach(Medicamento m in t.ListaMedicamentos) {
				dbcmd.CommandText = "INSERT INTO RECETAS (idTratamiento, idMedicamento) VALUES (@IdT, @idM)";
				dbcmd.Parameters.AddWithValue("@IdT", t.Id);
				dbcmd.Parameters.AddWithValue("@IdM", m.Id);

				dbcmd.ExecuteNonQuery();
			}

			Close();
		}

		/// <summary>
		/// Método que borra de la BD los datos del objeto de tipo Core.Tratamiento cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del tratamiento a borrar.</param>
		public void Delete(int id) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM TRATAMIENTOS WHERE idTratamiento = @Id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM RECETAS WHERE idTratamiento = @id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}
	}
}

