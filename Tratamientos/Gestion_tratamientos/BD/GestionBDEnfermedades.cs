﻿using System.Collections.Generic;
using Gestion_tratamientos.Core;

namespace Gestion_tratamientos.BD
{
	/// <summary>
	/// Clase encargada de realizar las inserciones, modificaciones, borrados y consultas de objetos de tipo <see cref="Core.Enfermedad"/> en la BD.
	/// <seealso cref="GestionBD"/>
	/// </summary>
    class GestionBDEnfermedades : BDConnection
    {
		/// <summary>
		/// Método encargado de cargar el diccionario de objetos de tipo Core.Enfermedad desde la BD.
		/// </summary>
        public SortedDictionary<int, Enfermedad> Load() {
            SortedDictionary<int, Enfermedad> dictionary = new SortedDictionary<int, Enfermedad>();

            Open();

            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "SELECT * FROM ENFERMEDADES";

            reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                int idenfermedad = reader.GetInt32(0);
                string nombreenfermedad = reader.GetString(1);
                string tipoenfermedad = reader.GetString(2);

                Enfermedad e = new Enfermedad(idenfermedad, nombreenfermedad, tipoenfermedad);

                dictionary.Add(idenfermedad, e);
            }

			reader.Close();
			reader = null;

            Close();

            return dictionary;
        }

		/// <summary>
		/// Metódo que recibe un objeto de tipo Core.Enfermedad e inserta sus datos en la BD.
		/// </summary>
		/// <param name="e">Objeto de tipo Core.Enfermedad.</param>
		public void Insert(Enfermedad e) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "INSERT INTO ENFERMEDADES (idEnfermedad, nombreEnfermedad, tipoEnfermedad) VALUES (@Id, @Nombre, @Tipo)";
			dbcmd.Parameters.AddWithValue("@Id", e.Id);
			dbcmd.Parameters.AddWithValue("@Nombre", e.Nombre);
			dbcmd.Parameters.AddWithValue("@Tipo", e.Tipo);

			dbcmd.ExecuteNonQuery();

			Close();
		}

		/// <summary>
		/// Método que recibe el id de una enfermedad y un objeto de tipo Core.Enfermedad y actualiza los datos de dicho objeto en la BD.
		/// </summary>
		/// <param name="id">Id de la enfermedad.</param>
		/// <param name="e">Objeto de tipo Core.Enfermedad.</param>
		public void Update(int id, Enfermedad e) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "UPDATE ENFERMEDADES SET nombreEnfermedad = @Nombre, tipoEnfermedad = @Tipo WHERE idEnfermedad = @id";
			dbcmd.Parameters.AddWithValue("@Nombre", e.Nombre);
			dbcmd.Parameters.AddWithValue("@Tipo", e.Tipo);
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}

		/// <summary>
		/// Método que borra de la BD los datos del objeto de tipo Core.Enfermedad cuyo id se pasa por parámetro.
		/// </summary>
		/// <param name="id">Id del enfermedad a borrar.</param>
		public void Delete(int id) {
			Open();

			dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = "DELETE FROM ENFERMEDADES WHERE idEnfermedad = @Id";
			dbcmd.Parameters.AddWithValue("@Id", id);

			dbcmd.ExecuteNonQuery();

			Close();
		}
    }
}
