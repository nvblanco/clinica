﻿using System;

namespace Gestion_tratamientos.Utils
{
	/// <summary>
	/// Clase que representa un elemento de un diccionario, utilizado en la vista para mantener los ids de los objetos en los comboBox y checkBox.
	/// </summary>
	public class Thing
	{
		/// <summary>
		/// Clave del elemento del diccionario.
		/// </summary>
		/// <value>La clave.</value>
		public string Key { 
			get; set; 
		}

		/// <summary>
		/// Valor del elemento del diccionario.
		/// </summary>
		/// <value>La clave.</value>
		public string Value { 
			get; set; 
		}

		/// <summary>
		/// Método que devuelve un objeto <see cref="System.String"/> que representa la instancia Thing actual.
		/// </summary>
		/// <returns>Un objeto <see cref="System.String"/> que representa la instancia Thing actual.</returns>
		public override string ToString() {
			return Value;
		}
	}
}

