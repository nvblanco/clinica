var searchData=
[
  ['obtener',['Obtener',['../classGestion__tratamientos_1_1Core_1_1GestionEnfermedades.html#a8aefe1a9ae5675cbb348bf27d972972f',1,'Gestion_tratamientos.Core.GestionEnfermedades.Obtener()'],['../classGestion__tratamientos_1_1Core_1_1GestionMedicamentos.html#a41e5484224f2a2d9e2f64d60d9f23e25',1,'Gestion_tratamientos.Core.GestionMedicamentos.Obtener()'],['../classGestion__tratamientos_1_1Core_1_1GestionPacientes.html#a66c1f95a5bcfdc7f840433fc2ebdddab',1,'Gestion_tratamientos.Core.GestionPacientes.Obtener()'],['../classGestion__tratamientos_1_1Core_1_1GestionTratamientos.html#aa918eaa86812a958a5f84374b109891e',1,'Gestion_tratamientos.Core.GestionTratamientos.Obtener()']]],
  ['obtenerenfermedad',['ObtenerEnfermedad',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a30845fde70f932f1e6205cf88bab5df8',1,'Gestion_tratamientos::Core::Controlador']]],
  ['obtenermedicamento',['ObtenerMedicamento',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#ae8cdf27c34f31b238cd4648398bc479a',1,'Gestion_tratamientos::Core::Controlador']]],
  ['obtenerpaciente',['ObtenerPaciente',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a267cedab38ea353a383c45daaa0ba2c7',1,'Gestion_tratamientos::Core::Controlador']]],
  ['obtenertratamiento',['ObtenerTratamiento',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#ae613f263cf7d2673cea71bb91304fde7',1,'Gestion_tratamientos::Core::Controlador']]],
  ['onquit',['OnQuit',['../classGestion__tratamientos_1_1View_1_1MainWindow.html#aae7b4ab4e9ea8caf67679c3dcce1ebab',1,'Gestion_tratamientos::View::MainWindow']]],
  ['open',['Open',['../classGestion__tratamientos_1_1BD_1_1BDConnection.html#aefcd32d40a0dd6f0e8373895f1e0cd66',1,'Gestion_tratamientos::BD::BDConnection']]]
];
