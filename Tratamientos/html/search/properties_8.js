var searchData=
[
  ['nombre',['Nombre',['../classGestion__tratamientos_1_1Core_1_1Enfermedad.html#acbc5d29e05089f4a698a72762a2ec61b',1,'Gestion_tratamientos.Core.Enfermedad.Nombre()'],['../classGestion__tratamientos_1_1Core_1_1Medicamento.html#a107ed2fd37388d34f2e65c197a06cda2',1,'Gestion_tratamientos.Core.Medicamento.Nombre()'],['../classGestion__tratamientos_1_1Core_1_1Paciente.html#aac7219fcde5fb55a465ed61632ed1218',1,'Gestion_tratamientos.Core.Paciente.Nombre()']]],
  ['numenfermedades',['NumEnfermedades',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a2136d1d29cb0b7368162eb44f4db81f9',1,'Gestion_tratamientos::Core::Controlador']]],
  ['nummedicamentos',['NumMedicamentos',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#abdcbc5948bc509b06d81bcd78aed65d5',1,'Gestion_tratamientos::Core::Controlador']]],
  ['numpacientes',['NumPacientes',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a7f914de7875dca2c63c89f9f953b95d7',1,'Gestion_tratamientos::Core::Controlador']]],
  ['numtratamientos',['NumTratamientos',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a49c54ccfba0bd9f8800f6cab9a263164',1,'Gestion_tratamientos::Core::Controlador']]]
];
