var searchData=
[
  ['gestionenfermedades',['gestionenfermedades',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a87d157510eab474e6815475389e62865',1,'Gestion_tratamientos::Core::Controlador']]],
  ['gestionmedicamentos',['gestionmedicamentos',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#aa2ced9579eeaee946e48d060d28398be',1,'Gestion_tratamientos::Core::Controlador']]],
  ['gestionpacientes',['gestionpacientes',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#a14d0067668aeafdc7b600f5bcc53ebab',1,'Gestion_tratamientos::Core::Controlador']]],
  ['gestiontratamientos',['gestiontratamientos',['../classGestion__tratamientos_1_1Core_1_1Controlador.html#abc0302444de2a42d21ee9a1529c31f62',1,'Gestion_tratamientos::Core::Controlador']]],
  ['grdlistaenfermedades',['grdListaEnfermedades',['../classGestion__tratamientos_1_1View_1_1EnfermedadesTab.html#aa236ada9cf6465c5a42068cd20f61c13',1,'Gestion_tratamientos::View::EnfermedadesTab']]],
  ['grdlistamedicamentos',['grdListaMedicamentos',['../classGestion__tratamientos_1_1View_1_1MedicamentosTab.html#a10ef21720ba3537295f658022aa53084',1,'Gestion_tratamientos::View::MedicamentosTab']]],
  ['grdlistapacientes',['grdListaPacientes',['../classGestion__tratamientos_1_1View_1_1PacientesTab.html#a65fcadc640bac427ec6c2528c1c3d4de',1,'Gestion_tratamientos::View::PacientesTab']]],
  ['grdlistatratamientos',['grdListaTratamientos',['../classGestion__tratamientos_1_1View_1_1TratamientosTab.html#a9393f2ef8856b13da3b278e7677f7861',1,'Gestion_tratamientos::View::TratamientosTab']]]
];
