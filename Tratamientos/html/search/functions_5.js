var searchData=
[
  ['gestionenfermedades',['GestionEnfermedades',['../classGestion__tratamientos_1_1Core_1_1GestionEnfermedades.html#a5a7609d02f569a78a85e88e1812ed474',1,'Gestion_tratamientos::Core::GestionEnfermedades']]],
  ['gestionmedicamentos',['GestionMedicamentos',['../classGestion__tratamientos_1_1Core_1_1GestionMedicamentos.html#a2ade34dade25bf40b8d64fbc43208864',1,'Gestion_tratamientos::Core::GestionMedicamentos']]],
  ['gestionpacientes',['GestionPacientes',['../classGestion__tratamientos_1_1Core_1_1GestionPacientes.html#a1df8df411d69cdfec4973b091e955277',1,'Gestion_tratamientos::Core::GestionPacientes']]],
  ['gestiontratamientos',['GestionTratamientos',['../classGestion__tratamientos_1_1Core_1_1GestionTratamientos.html#a3f1d17080d6757475d89bb0631b45038',1,'Gestion_tratamientos::Core::GestionTratamientos']]],
  ['getidenfermedadseleccionada',['GetIdEnfermedadSeleccionada',['../classGestion__tratamientos_1_1View_1_1EnfermedadesTab.html#a327388c2a94c71248d8ff6e55ec404f8',1,'Gestion_tratamientos::View::EnfermedadesTab']]],
  ['getidmedicamentoseleccionado',['GetIdMedicamentoSeleccionado',['../classGestion__tratamientos_1_1View_1_1MedicamentosTab.html#a42b408e4f895d58ab3095a73207edfad',1,'Gestion_tratamientos::View::MedicamentosTab']]],
  ['getidpacienteseleccionado',['GetIdPacienteSeleccionado',['../classGestion__tratamientos_1_1View_1_1PacientesTab.html#a9d05fe6c78aab6b509d107dbe07374f1',1,'Gestion_tratamientos::View::PacientesTab']]],
  ['getlastid',['getLastId',['../classGestion__tratamientos_1_1Core_1_1GestionEnfermedades.html#ad3aba95e785ada7ebd6fbf4e94bd006f',1,'Gestion_tratamientos.Core.GestionEnfermedades.getLastId()'],['../classGestion__tratamientos_1_1Core_1_1GestionMedicamentos.html#a487e430ccccee129e554909435b1870c',1,'Gestion_tratamientos.Core.GestionMedicamentos.getLastId()'],['../classGestion__tratamientos_1_1Core_1_1GestionPacientes.html#ae27200e5a573b39769a4fbf6622cdf74',1,'Gestion_tratamientos.Core.GestionPacientes.getLastId()'],['../classGestion__tratamientos_1_1Core_1_1GestionTratamientos.html#aaa482e99cd882d3400bf24913b1d519b',1,'Gestion_tratamientos.Core.GestionTratamientos.getLastId()']]]
];
