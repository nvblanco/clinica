var searchData=
[
  ['tostring',['ToString',['../classGestion__tratamientos_1_1Utils_1_1Thing.html#ab702f992401516e36c3130255176ebdb',1,'Gestion_tratamientos::Utils::Thing']]],
  ['tratamiento',['Tratamiento',['../classGestion__tratamientos_1_1Core_1_1Tratamiento.html#a61e69311085dbd807c8eabb6644c4c7b',1,'Gestion_tratamientos.Core.Tratamiento.Tratamiento(int duracion_dias, List&lt; Medicamento &gt; medicamentos, Enfermedad enfermedad, Paciente paciente)'],['../classGestion__tratamientos_1_1Core_1_1Tratamiento.html#aec3e565c92ea9b8cea5ff3c5f10ad33a',1,'Gestion_tratamientos.Core.Tratamiento.Tratamiento(int id, int duracion_dias, List&lt; Medicamento &gt; medicamentos, Enfermedad enfermedad, Paciente paciente)']]],
  ['tratamientostab',['TratamientosTab',['../classGestion__tratamientos_1_1View_1_1TratamientosTab.html#a9bab532933a26cae8f751827d2e2534c',1,'Gestion_tratamientos::View::TratamientosTab']]]
];
